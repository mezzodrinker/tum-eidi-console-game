==============
 CONSOLE-GAME
==============

This game requires Java 8 Update 25 or later. If you don't have Java 8 yet, please 
download it from this location:

	http://www.oracle.com/technetwork/java/javase/downloads/index.html

In case you're not sure about which Java version is installed on your device, visit
this site:

	https://www.java.com/en/download/installed.jsp

If the game is not working for you, use your system's console and run the game by
navigating into the directory you put it into and typing

	java -jar console-game-0.0.2-alpha.jar

You should be able to see any errors occurring in any case.

Game on!

-- mezzo, Lead Developer