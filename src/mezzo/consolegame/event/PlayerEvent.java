/*
 * LICENSE
 * 
 * This work is licensed under the Creative Commons Attribution-ShareAlike 4.0
 * International License. To view a copy of this license, visit
 * http://creativecommons.org/licenses/by-sa/4.0/.
 */
package mezzo.consolegame.event;

import mezzo.consolegame.entity.Player;

/**
 * <code>PlayerEvent</code>
 * 
 * @author Felix Fr�hlich <felix.froehlich@convey.de>
 */
public class PlayerEvent extends EntityEvent {
    protected Player player = null;

    public PlayerEvent(Player player) {
        super(player);
        this.player = player;
    }

    public Player getPlayer() {
        return player;
    }
}
