/*
 * LICENSE
 * 
 * This work is licensed under the Creative Commons Attribution-ShareAlike 4.0
 * International License. To view a copy of this license, visit
 * http://creativecommons.org/licenses/by-sa/4.0/.
 */
package mezzo.consolegame.event.handler;

import mezzo.consolegame.Level;
import mezzo.consolegame.entity.Monster;
import mezzo.consolegame.entity.Player;
import mezzo.consolegame.event.EntityCollisionEvent;
import mezzo.consolegame.event.EntityDeathEvent;
import mezzo.consolegame.event.EntityHealedEvent;
import mezzo.consolegame.event.EntityHitEvent;
import mezzo.consolegame.event.EntityMovementEvent;
import mezzo.consolegame.event.EntityStepOnTrapEvent;
import mezzo.consolegame.gui.ConsoleFrame;
import mezzo.consolegame.listener.EntityListener;

/**
 * <code>MonsterHandler</code>
 * 
 * @author Felix Fr�hlich <felix.froehlich@convey.de>
 */
public class MonsterHandler implements EntityListener {
    private final ConsoleFrame consoleFrame;

    public MonsterHandler(ConsoleFrame consoleFrame) {
        this.consoleFrame = consoleFrame;
    }

    @Override
    public void onEntityCollision(EntityCollisionEvent e) {
        if (e.getOtherEntity() instanceof Player && e.getEntity() instanceof Monster) {
            e.getEntity().damage(1, e.getOtherEntity());
            e.getEntity().forceReprint();
        }
    }

    @Override
    public void onEntityMovement(EntityMovementEvent e) {}

    @Override
    public void onEntityHit(EntityHitEvent e) {
        if (e.getEntity().getHealth() <= 0) {
            if (e.getCauseEntity() != null) {
                e.getEntity().fireEntityDeathEvent(new EntityDeathEvent(e.getEntity(), e.getCauseEntity()));
            } else if (e.getCauseItem() != null) {
                e.getEntity().fireEntityDeathEvent(new EntityDeathEvent(e.getEntity(), e.getCauseItem()));
            } else {
                e.getEntity().fireEntityDeathEvent(new EntityDeathEvent(e.getEntity(), e.getCauseLevelComponent()));
            }
        }
    }

    @Override
    public void onEntityStepOnTrap(EntityStepOnTrapEvent e) {
        e.getEntity().damage(e.getTrap().getDamage(), e.getTrap());
    }

    @Override
    public void onEntityDeath(EntityDeathEvent e) {
        if (e.getCauseEntity() != null) {
            System.out.println(e.getEntity() + " was killed by " + e.getCauseEntity());
        } else if (e.getCauseItem() != null) {
            System.out.println(e.getEntity() + " was killed by " + e.getCauseItem());
        } else {
            System.out.println(e.getEntity() + " was killed by " + e.getCauseLevelComponent());
        }
        consoleFrame.getLevel().removeEntity(e.getEntity());
        consoleFrame.getLevel().set(e.getEntity().getPosition().x, e.getEntity().getPosition().y, Level.MONSTER_CORPSE);
        if (e.getCauseEntity() instanceof Player) {
            ((Player) e.getCauseEntity()).addKills(1);
        }
    }

    @Override
    public void onEntityHealed(EntityHealedEvent e) {}
}
