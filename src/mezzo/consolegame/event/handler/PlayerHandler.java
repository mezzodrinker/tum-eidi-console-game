/*
 * LICENSE
 * 
 * This work is licensed under the Creative Commons Attribution-ShareAlike 4.0
 * International License. To view a copy of this license, visit
 * http://creativecommons.org/licenses/by-sa/4.0/.
 */
package mezzo.consolegame.event.handler;

import mezzo.consolegame.Level;
import mezzo.consolegame.entity.Monster;
import mezzo.consolegame.entity.Player;
import mezzo.consolegame.event.EntityCollisionEvent;
import mezzo.consolegame.event.EntityDeathEvent;
import mezzo.consolegame.event.EntityHealedEvent;
import mezzo.consolegame.event.EntityHitEvent;
import mezzo.consolegame.event.EntityMovementEvent;
import mezzo.consolegame.event.EntityStepOnTrapEvent;
import mezzo.consolegame.event.PlayerDeathEvent;
import mezzo.consolegame.event.PlayerLootCollectEvent;
import mezzo.consolegame.event.PlayerReachedExitEvent;
import mezzo.consolegame.gui.ConsoleFrame;
import mezzo.consolegame.item.Key;
import mezzo.consolegame.listener.PlayerListener;

/**
 * <code>PlayerHandler</code>
 * 
 * @author Felix Fr�hlich <felix.froehlich@convey.de>
 */
public class PlayerHandler implements PlayerListener {
    private final ConsoleFrame consoleFrame;

    public PlayerHandler(ConsoleFrame consoleFrame) {
        this.consoleFrame = consoleFrame;
    }

    @Override
    public void onEntityCollision(EntityCollisionEvent e) {
        if (e.getOtherEntity() instanceof Monster && e.getEntity() instanceof Player) {
            e.getEntity().damage(1, e.getOtherEntity());
            e.getEntity().forceReprint();
        }
    }

    @Override
    public void onEntityMovement(EntityMovementEvent e) {
        consoleFrame.movement();
    }

    @Override
    public void onEntityHit(EntityHitEvent e) {
        if (e.getEntity() instanceof Player) {
            Player player = (Player) e.getEntity();
            if (player.getHealth() <= 0) {
                if (e.getCauseEntity() != null) {
                    player.firePlayerDeathEvent(new PlayerDeathEvent(player, e.getCauseEntity()));
                } else if (e.getCauseItem() != null) {
                    player.firePlayerDeathEvent(new PlayerDeathEvent(player, e.getCauseItem()));
                } else {
                    player.firePlayerDeathEvent(new PlayerDeathEvent(player, e.getCauseLevelComponent()));
                }
            }
        }
    }

    @Override
    public void onEntityHealed(EntityHealedEvent e) {}

    @Override
    public void onPlayerLootCollect(PlayerLootCollectEvent e) {
        e.getPlayer().getInventory().add(e.getLoot().getContents());
        e.getLevel().set(e.getPosition().x, e.getPosition().y, Level.AIR);
    }

    @Override
    public void onPlayerReachedExit(PlayerReachedExitEvent e) {
        if (e.getPlayer().getInventory().get(Key.class) > 0) {
            e.setCancelled(true);
            consoleFrame.win();
        }
    }

    @Override
    public void onEntityStepOnTrap(EntityStepOnTrapEvent e) {
        if (e.getEntity() instanceof Player) {
            e.getEntity().damage(e.getTrap().getDamage(), e.getTrap());
        }
    }

    @Override
    public void onPlayerDeath(PlayerDeathEvent e) {
        if (e.getCauseEntity() != null) {
            System.out.println(e.getPlayer() + " was killed by " + e.getCauseEntity());
        } else if (e.getCauseItem() != null) {
            System.out.println(e.getPlayer() + " was killed by " + e.getCauseItem());
        } else {
            System.out.println(e.getPlayer() + " was killed by " + e.getCauseLevelComponent());
        }
        e.setCancelled(true);
        consoleFrame.lose();
    }

    @Override
    public void onEntityDeath(EntityDeathEvent e) {
        if (e.getEntity() instanceof Player) {
            if (e.getCauseEntity() != null) {
                System.out.println(e.getEntity() + " was killed by " + e.getCauseEntity());
            } else if (e.getCauseItem() != null) {
                System.out.println(e.getEntity() + " was killed by " + e.getCauseItem());
            } else {
                System.out.println(e.getEntity() + " was killed by " + e.getCauseLevelComponent());
            }
            e.setCancelled(true);
            consoleFrame.lose();
        }
    }
}