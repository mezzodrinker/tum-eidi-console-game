/*
 * LICENSE
 * 
 * This work is licensed under the Creative Commons Attribution-ShareAlike 4.0
 * International License. To view a copy of this license, visit
 * http://creativecommons.org/licenses/by-sa/4.0/.
 */
package mezzo.consolegame.event;

import mezzo.consolegame.entity.Entity;
import mezzo.consolegame.item.Item;
import mezzo.consolegame.level.LevelComponent;

/**
 * <code>EntityHitEvent</code>
 * 
 * @author Felix Fr�hlich <felix.froehlich@convey.de>
 */
public class EntityHitEvent extends EntityEvent {
    protected Entity         causeEntity         = null;
    protected Item           causeItem           = null;
    protected LevelComponent causeLevelComponent = null;
    protected int            damage              = 0;

    public EntityHitEvent(Entity entity, Entity cause, int damage) {
        super(entity);
        causeEntity = cause;
        this.damage = damage;
    }

    public EntityHitEvent(Entity entity, Item cause, int damage) {
        super(entity);
        causeItem = cause;
        this.damage = damage;
    }

    public EntityHitEvent(Entity entity, LevelComponent cause, int damage) {
        super(entity);
        causeLevelComponent = cause;
        this.damage = damage;
    }

    public Entity getCauseEntity() {
        return causeEntity;
    }

    public Item getCauseItem() {
        return causeItem;
    }

    public LevelComponent getCauseLevelComponent() {
        return causeLevelComponent;
    }

    public int getDamage() {
        return damage;
    }
}
