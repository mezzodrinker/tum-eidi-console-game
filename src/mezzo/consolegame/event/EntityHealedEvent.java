/*
 * LICENSE
 * 
 * This work is licensed under the Creative Commons Attribution-ShareAlike 4.0
 * International License. To view a copy of this license, visit
 * http://creativecommons.org/licenses/by-sa/4.0/.
 */
package mezzo.consolegame.event;

import mezzo.consolegame.entity.Entity;
import mezzo.consolegame.item.Item;

/**
 * <code>EntityHealedEvent</code>
 * 
 * @author Felix Fr�hlich <felix.froehlich@convey.de>
 */
public class EntityHealedEvent extends EntityEvent {
    protected Entity causeEntity = null;
    protected Item   causeItem   = null;
    protected int    amount      = 0;

    public EntityHealedEvent(Entity entity, Entity cause, int amount) {
        super(entity);
        causeEntity = cause;
        this.amount = amount;
    }

    public EntityHealedEvent(Entity entity, Item cause, int amount) {
        super(entity);
        causeItem = cause;
        this.amount = amount;
    }

    public Entity getCauseEntity() {
        return causeEntity;
    }

    public Item getCauseItem() {
        return causeItem;
    }

    public int getAmount() {
        return amount;
    }
}
