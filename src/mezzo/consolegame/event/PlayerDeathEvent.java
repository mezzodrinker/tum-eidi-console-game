/*
 * LICENSE
 * 
 * This work is licensed under the Creative Commons Attribution-ShareAlike 4.0
 * International License. To view a copy of this license, visit
 * http://creativecommons.org/licenses/by-sa/4.0/.
 */
package mezzo.consolegame.event;

import mezzo.consolegame.entity.Entity;
import mezzo.consolegame.entity.Player;
import mezzo.consolegame.item.Item;
import mezzo.consolegame.level.LevelComponent;

/**
 * <code>PlayerDeathEvent</code>
 * 
 * @author Felix Fr�hlich <felix.froehlich@convey.de>
 */
public class PlayerDeathEvent extends PlayerEvent {
    protected Entity         causeEntity         = null;
    protected Item           causeItem           = null;
    protected LevelComponent causeLevelComponent = null;

    public PlayerDeathEvent(Player player, Entity cause) {
        super(player);
        causeEntity = cause;
    }

    public PlayerDeathEvent(Player player, Item cause) {
        super(player);
        causeItem = cause;
    }

    public PlayerDeathEvent(Player player, LevelComponent cause) {
        super(player);
        causeLevelComponent = cause;
    }

    public Entity getCauseEntity() {
        return causeEntity;
    }

    public Item getCauseItem() {
        return causeItem;
    }

    public LevelComponent getCauseLevelComponent() {
        return causeLevelComponent;
    }
}
