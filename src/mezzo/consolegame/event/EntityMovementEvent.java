/*
 * LICENSE
 * 
 * This work is licensed under the Creative Commons Attribution-ShareAlike 4.0
 * International License. To view a copy of this license, visit
 * http://creativecommons.org/licenses/by-sa/4.0/.
 */
package mezzo.consolegame.event;

import mezzo.consolegame.Level;
import mezzo.consolegame.Level.Point;
import mezzo.consolegame.entity.Entity;

/**
 * <code>MovementEvent</code>
 * 
 * @author Felix Fr�hlich <felix.froehlich@convey.de>
 */
public class EntityMovementEvent extends EntityEvent {
    protected Point from  = null;
    protected Point to    = null;
    protected Level level = null;

    public EntityMovementEvent(Entity entity, Point from, Point to, Level level) {
        super(entity);
        this.from = from;
        this.to = to;
        this.level = level;
    }

    public Point getFrom() {
        return from;
    }

    public Point getTo() {
        return to;
    }

    public Level getLevel() {
        return level;
    }
}
