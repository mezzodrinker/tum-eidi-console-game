/*
 * LICENSE
 * 
 * This work is licensed under the Creative Commons Attribution-ShareAlike 4.0
 * International License. To view a copy of this license, visit
 * http://creativecommons.org/licenses/by-sa/4.0/.
 */
package mezzo.consolegame.event;

import mezzo.consolegame.entity.Entity;

/**
 * <code>CollisionEvent</code>
 * 
 * @author Felix Fr�hlich <felix.froehlich@convey.de>
 */
public class EntityCollisionEvent extends EntityEvent {
    protected Entity otherEntity = null;

    public EntityCollisionEvent(Entity entity, Entity otherEntity) {
        super(entity);
        this.otherEntity = otherEntity;
    }

    public Entity getOtherEntity() {
        return otherEntity;
    }
}
