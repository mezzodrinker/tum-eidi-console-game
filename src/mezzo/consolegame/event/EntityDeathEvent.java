/*
 * LICENSE
 * 
 * This work is licensed under the Creative Commons Attribution-ShareAlike 4.0
 * International License. To view a copy of this license, visit
 * http://creativecommons.org/licenses/by-sa/4.0/.
 */
package mezzo.consolegame.event;

import mezzo.consolegame.entity.Entity;
import mezzo.consolegame.item.Item;
import mezzo.consolegame.level.LevelComponent;

/**
 * <code>EntityDeathEvent</code>
 * 
 * @author Felix Fr�hlich <felix.froehlich@convey.de>
 */
public class EntityDeathEvent extends EntityEvent {
    protected Entity         causeEntity         = null;
    protected Item           causeItem           = null;
    protected LevelComponent causeLevelComponent = null;

    public EntityDeathEvent(Entity entity, Entity cause) {
        super(entity);
        causeEntity = cause;
    }

    public EntityDeathEvent(Entity entity, Item cause) {
        super(entity);
        causeItem = cause;
    }

    public EntityDeathEvent(Entity entity, LevelComponent cause) {
        super(entity);
        causeLevelComponent = cause;
    }

    public Entity getCauseEntity() {
        return causeEntity;
    }

    public Item getCauseItem() {
        return causeItem;
    }

    public LevelComponent getCauseLevelComponent() {
        return causeLevelComponent;
    }
}
