/*
 * LICENSE
 * 
 * This work is licensed under the Creative Commons Attribution-ShareAlike 4.0
 * International License. To view a copy of this license, visit
 * http://creativecommons.org/licenses/by-sa/4.0/.
 */
package mezzo.consolegame.event;

import mezzo.consolegame.Level;
import mezzo.consolegame.Level.Point;
import mezzo.consolegame.entity.Player;
import mezzo.consolegame.level.Loot;

/**
 * <code>LootCollectEvent</code>
 * 
 * @author Felix Fr�hlich <felix.froehlich@convey.de>
 */
public class PlayerLootCollectEvent extends PlayerEvent {
    protected Point position = null;
    protected Loot  loot     = null;
    protected Level level    = null;

    public PlayerLootCollectEvent(Player player, Loot loot, Point position, Level level) {
        super(player);
        this.loot = loot;
        this.position = position;
        this.level = level;
    }

    public Point getPosition() {
        return position;
    }

    public Loot getLoot() {
        return loot;
    }

    public Level getLevel() {
        return level;
    }
}
