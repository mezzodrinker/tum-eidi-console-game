/*
 * LICENSE
 * 
 * This work is licensed under the Creative Commons Attribution-ShareAlike 4.0
 * International License. To view a copy of this license, visit
 * http://creativecommons.org/licenses/by-sa/4.0/.
 */
package mezzo.consolegame.event;

import mezzo.consolegame.Level;
import mezzo.consolegame.Level.Point;
import mezzo.consolegame.entity.Entity;
import mezzo.consolegame.level.Trap;

/**
 * <code>EntityStepOnTrapEvent</code>
 * 
 * @author Felix Fr�hlich <felix.froehlich@convey.de>
 */
public class EntityStepOnTrapEvent extends EntityEvent {
    protected Point position = null;
    protected Level level    = null;
    protected Trap  trap     = null;

    public EntityStepOnTrapEvent(Entity entity, Trap trap, Point position, Level level) {
        super(entity);
        this.position = position;
        this.level = level;
        this.trap = trap;
    }

    public Point getPosition() {
        return position;
    }

    public Level getLevel() {
        return level;
    }

    public Trap getTrap() {
        return trap;
    }
}
