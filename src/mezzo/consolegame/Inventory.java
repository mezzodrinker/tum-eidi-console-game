/*
 * LICENSE
 * 
 * This work is licensed under the Creative Commons Attribution-ShareAlike 4.0
 * International License. To view a copy of this license, visit
 * http://creativecommons.org/licenses/by-sa/4.0/.
 */
package mezzo.consolegame;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import mezzo.consolegame.gui.Alignment;
import mezzo.consolegame.gui.Terminal;
import mezzo.consolegame.gui.TerminalSection;
import mezzo.consolegame.item.Item;

/**
 * <code>Inventory</code>
 * 
 * @author Felix Fr�hlich <felix.froehlich@convey.de>
 */
public class Inventory {
    private boolean               changed = true;
    private List<Item>            items   = new ArrayList<Item>();
    private final TerminalSection section;

    public Inventory(Terminal terminal) {
        section = terminal.createSection(0, 1, terminal.getWidth(), 1);
    }

    public int get(Class<? extends Item> c) {
        return getItemCount(c);
    }

    public void add(Collection<? extends Item> items) {
        for (Item item : items) {
            add(item);
        }
    }

    public void add(Item item) {
        items.add(item);
        changed = true;
    }

    public boolean remove(Class<? extends Item> c) {
        for (Item item : items) {
            if (c.isInstance(item)) {
                boolean success = items.remove(item);
                changed = changed || success;
                return success;
            }
        }
        return false;
    }

    public boolean hasChanged() {
        return changed;
    }

    public void forceUpdate() {
        changed = true;
    }

    public boolean rearm() {
        boolean b = changed;
        changed = false;
        return b;
    }

    public Map<Class<? extends Item>, Integer> getItemCount() {
        Map<Class<? extends Item>, Integer> count = new HashMap<Class<? extends Item>, Integer>();
        for (Item item : items) {
            Integer i = count.get(item.getClass());
            if (i == null) {
                count.put(item.getClass(), 1);
                continue;
            }
            count.put(item.getClass(), i + 1);
        }
        return count;
    }

    public int getItemCount(Class<? extends Item> c) {
        return getItemCount().getOrDefault(c, 0);
    }

    public void print() {
        if (items.isEmpty()) return;
        StringBuilder sb = new StringBuilder("Inventory:");
        Map<Class<? extends Item>, Integer> count = getItemCount();
        for (Entry<Class<? extends Item>, Integer> entry : count.entrySet()) {
            sb.append(" ").append(entry.getValue()).append(" ");
            try {
                if (entry.getValue() == 1) {
                    sb.append(entry.getKey().newInstance().getNameSingular());
                } else {
                    sb.append(entry.getKey().newInstance().getNamePlural());
                }
            } catch (IllegalAccessException | InstantiationException e) {
                e.printStackTrace();
            }
        }
        section.print(sb.toString(), Alignment.LEFT);
    }
}
