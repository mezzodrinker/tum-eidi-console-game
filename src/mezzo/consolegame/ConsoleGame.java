/*
 * LICENSE
 * 
 * This work is licensed under the Creative Commons Attribution-ShareAlike 4.0
 * International License. To view a copy of this license, visit
 * http://creativecommons.org/licenses/by-sa/4.0/.
 */
package mezzo.consolegame;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

import mezzo.consolegame.Level.Point;
import mezzo.consolegame.Level.PointNode;
import mezzo.consolegame.entity.Monster;
import mezzo.consolegame.gui.Alignment;
import mezzo.consolegame.gui.ConsoleFrame;
import mezzo.consolegame.gui.Terminal;
import mezzo.consolegame.level.LevelComponent;
import mezzo.consolegame.level.generation.LevelGenerator;
import mezzo.consolegame.level.generation.LevelGenerator.GeneratorSettings;
import mezzo.util.BidirectionalMap;
import mezzo.util.HashTable;
import mezzo.util.ReturnValue2;
import mezzo.util.ReturnValue3;
import mezzo.util.logging.file.DefaultFileLogger;
import mezzo.util.time.Time;

import com.googlecode.lanterna.terminal.Terminal.Color;

/**
 * <code>ConsoleGame</code>
 * 
 * @author Felix Fr�hlich <felix.froehlich@convey.de>
 */
public class ConsoleGame {
    private ConsoleFrame             consoleFrame   = null;

    private static final File        SAVEGAME       = new File("savegame.properties");
    private static final File        SHORTCUTS      = new File("metadata.properties");
    private static final File        LOG            = new File("log.txt");
    private static DefaultFileLogger LOGGER         = null;

    public static final boolean      isDebugEnabled = true;

    public ConsoleGame() throws IOException {
        try {
            consoleFrame = new ConsoleFrame(this);
        } catch (Throwable t) {
            logger().log("Caught " + t.getClass().getName(), t);
        }
    }

    public static DefaultFileLogger logger() {
        if (LOGGER == null) {
            try {
                LOGGER = new DefaultFileLogger(LOG);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return LOGGER;
    }

    public Level loadLevel() {
        if (!SAVEGAME.exists()) return newLevel();

        try {
            Properties level = new Properties();
            level.load(new FileInputStream(SAVEGAME));

            Properties shortcuts = new Properties();
            if (SHORTCUTS.exists()) {
                level.load(new FileInputStream(SHORTCUTS));
            }

            return new Level(level, shortcuts, consoleFrame);
        } catch (IOException e) {
            logger().log("Error while loading level from " + SAVEGAME, e);
            throw new RuntimeException("failed to load level from " + SAVEGAME, e);
        }
    }

    public Level newLevel() {
        try {
            // TODO load settings from file
            GeneratorSettings g = new GeneratorSettings();
            g.width = 200;
            g.height = 200;
            g.complexity = 1d;
            g.countEntrances = 5;
            g.countExits = 2;
            g.countChests = 15;
            g.countTraps = 10;
            g.countMonsters = 5;
            ReturnValue3<LevelComponent[][], BidirectionalMap<Monster, Point>, HashTable<Point, Point, PointNode[]>> rv3 = LevelGenerator.generate(g);
            return new Level(rv3.value1, rv3.value2, rv3.value3, consoleFrame, g);
        } catch (Throwable t) {
            Terminal terminal = consoleFrame.getTerminal();
            terminal.setBackground(Color.RED).print();
            terminal.setForeground(Color.WHITE).print(t.getClass().getSimpleName() + " while generating new game: " + t.getMessage(), Alignment.LEFT);
            t.printStackTrace();
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
            }
            System.exit(1);
            return null;
        }
    }

    public void save(Level level) {
        try {
            ReturnValue2<Properties, Properties> rv2 = Level.toProperties(level);
            rv2.value1.store(new FileWriter(SAVEGAME), "Save state from " + Time.getCurrentTimeAsString());
            rv2.value2.store(new FileWriter(SHORTCUTS), "Save state from " + Time.getCurrentTimeAsString());
        } catch (IOException e) {
            throw new RuntimeException("failed to save level to " + SAVEGAME, e);
        }
    }

    public void exit() {
        System.exit(0);
    }

    public static void main(String[] args) {
        try {
            if (LOG.exists()) {
                LOG.delete();
            }
            new ConsoleGame();
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }
}
