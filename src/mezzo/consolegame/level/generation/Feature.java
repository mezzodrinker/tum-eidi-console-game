/*
 * LICENSE
 * 
 * This work is licensed under the Creative Commons Attribution-ShareAlike 4.0
 * International License. To view a copy of this license, visit
 * http://creativecommons.org/licenses/by-sa/4.0/.
 */
package mezzo.consolegame.level.generation;

import java.util.Set;

import mezzo.consolegame.Level.Point;
import mezzo.consolegame.Level.PointNode;

/**
 * <code>Feature</code>
 * 
 * @author Felix Fr�hlich <felix.froehlich@convey.de>
 */
public interface Feature {
    public int getX();

    public int getY();

    public int getWidth();

    public int getHeight();

    public Set<Point> getAdjacentWalls();

    public Set<Point> getContainedPoints();

    public Set<Point> getEntrances();

    public void addEntrance(Point point);

    public PointNode[] getPath(Point from, Point to);

    public default Direction getDirection(int x, int y) {
        return getDirection(new Point(x, y));
    }

    public default Direction getDirection(Point point) {
        if (point.y < getY()) return Direction.UP;
        if (point.y >= getY() + getHeight()) return Direction.DOWN;
        if (point.x < getX()) return Direction.LEFT;
        if (point.x >= getX() + getWidth()) return Direction.RIGHT;
        return Direction.NONE;
    }

    public default boolean intersects(Feature other) {
        int width = getWidth();
        int height = getHeight();
        int otherWidth = other.getWidth();
        int otherHeight = other.getHeight();
        otherWidth += other.getX() + 1;
        otherHeight += other.getY() + 1;
        width += getX() + 1;
        height += getY() + 1;
        // overflow-conscious code
        return ((otherWidth < other.getX() || otherWidth > getX()) && (otherHeight < other.getY() || otherHeight > getY()) && (width < getX() || width > other.getX()) && (height < getY() || height > other
                .getY()));
    }

    public static enum Direction {
        UP, RIGHT, DOWN, LEFT, NONE;
    }
}
