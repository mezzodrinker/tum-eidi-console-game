/*
 * LICENSE
 * 
 * This work is licensed under the Creative Commons Attribution-ShareAlike 4.0
 * International License. To view a copy of this license, visit
 * http://creativecommons.org/licenses/by-sa/4.0/.
 */
package mezzo.consolegame.level.generation;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Set;

import mezzo.consolegame.Level.Point;
import mezzo.consolegame.Level.PointNode;
import mezzo.consolegame.entity.Monster;
import mezzo.consolegame.level.Air;
import mezzo.consolegame.level.Entrance;
import mezzo.consolegame.level.Exit;
import mezzo.consolegame.level.LevelComponent;
import mezzo.consolegame.level.Loot;
import mezzo.consolegame.level.Trap;
import mezzo.consolegame.level.Wall;
import mezzo.consolegame.level.generation.Feature.Direction;
import mezzo.util.BidirectionalMap;
import mezzo.util.HashTable;
import mezzo.util.ReturnValue2;
import mezzo.util.ReturnValue3;

/**
 * <code>LevelGenerator</code>
 * 
 * @author Felix Fr�hlich <felix.froehlich@convey.de>
 */
public class LevelGenerator {
    private static Random                           random   = null;
    private static List<Feature>                    features = null;
    private static BidirectionalMap<Monster, Point> monsters = null;

    private static void initialize(GeneratorSettings g) {
        g.validate();
        random = new Random(g.seed);
        features = new ArrayList<>();
        monsters = new BidirectionalMap<>();
    }

    private static void fill(LevelComponent[][] map, LevelComponent component) {
        for (int x = 0; x < map.length; x++) {
            for (int y = 0; y < map[x].length; y++) {
                map[x][y] = component;
            }
        }
    }

    private static void createFirstRoom(LevelComponent[][] map, GeneratorSettings g) {
        int roomWidth = Math.min(g.width, random.nextInt(g.roomWidthBounds) + g.roomWidthAdd);
        int roomHeight = Math.min(g.height, random.nextInt(g.roomHeightBounds) + g.roomHeightAdd);
        int roomX = (g.width - roomWidth) / 2;
        int roomY = (g.height - roomHeight) / 2;
        Room room = new Room(roomX, roomY, roomWidth, roomHeight);

        for (int x = room.x; x < room.x + room.width; x++) {
            for (int y = room.y; y < room.y + room.height; y++) {
                map[x][y] = new Air();
            }
        }

        features.add(room);
    }

    private static Feature getFeature() {
        return features.get(random.nextInt(features.size()));
    }

    private static ReturnValue2<Point, Feature> getWall() {
        Feature feature = getFeature();
        Set<Point> w = feature.getAdjacentWalls();
        Point[] walls = w.toArray(new Point[0]);
        return new ReturnValue2<>(walls[random.nextInt(walls.length)], feature);
    }

    private static ReturnValue2<Point, Feature> getAir() {
        Feature feature = getFeature();
        Set<Point> p = feature.getContainedPoints();
        Point[] points = p.toArray(new Point[0]);
        return new ReturnValue2<>(points[random.nextInt(points.length)], feature);
    }

    private static boolean createFeature(LevelComponent[][] map, GeneratorSettings g) {
        // pick a random wall
        ReturnValue2<Point, Feature> rv2 = getWall();
        Point wallLocation = rv2.value1;
        Feature wallFeature = rv2.value2;
        Direction direction = wallFeature.getDirection(wallLocation);

        // if direction is NONE, something went wrong
        if (direction == Direction.NONE) {
            System.err.println("Something went wrong in createFeature(LevelComponent[][], int, int): direction is NONE");
            return false;
        }

        // pick a random feature
        Feature feature = null;
        int r = random.nextInt(2);
        if (r == 0) {
            // new feature will be a room
            int roomWidth = Math.min(g.width, random.nextInt(g.roomWidthBounds) + g.roomWidthAdd);
            int roomHeight = Math.min(g.height, random.nextInt(g.roomHeightBounds) + g.roomHeightAdd);
            int roomX;
            int roomY;

            switch (direction) {
                case DOWN:
                    roomX = wallLocation.x - random.nextInt(roomWidth);
                    roomY = wallLocation.y + 1;
                    break;
                case LEFT:
                    roomX = wallLocation.x - roomWidth;
                    roomY = wallLocation.y - random.nextInt(roomHeight);
                    break;
                case RIGHT:
                    roomX = wallLocation.x + 1;
                    roomY = wallLocation.y - random.nextInt(roomHeight);
                    break;
                case UP:
                    roomX = wallLocation.x - random.nextInt(roomWidth);
                    roomY = wallLocation.y - roomHeight;
                    break;
                default:
                    System.err.println("Something went wrong in createFeature(LevelComponent[][], int, int): direction is NONE");
                    return false;
            }

            feature = new Room(roomX, roomY, roomWidth, roomHeight);
        } else if (r == 1) {
            // new feature will be a corridor
            boolean isVertical = random.nextBoolean();
            int i = random.nextInt(g.corridorLengthBounds) + g.corridorLengthAdd;
            int length = isVertical ? Math.min(g.height, i) : Math.min(g.width, i);
            int corridorX;
            int corridorY;

            switch (direction) {
                case DOWN:
                    if (isVertical) {
                        corridorX = wallLocation.x;
                        corridorY = wallLocation.y + 1;
                    } else {
                        corridorX = wallLocation.x - random.nextInt(length);
                        corridorY = wallLocation.y + 1;
                    }
                    break;
                case LEFT:
                    if (isVertical) {
                        corridorX = wallLocation.x - 1;
                        corridorY = wallLocation.y - random.nextInt(length);
                    } else {
                        corridorX = wallLocation.x - length;
                        corridorY = wallLocation.y;
                    }
                    break;
                case RIGHT:
                    if (isVertical) {
                        corridorX = wallLocation.x + 1;
                        corridorY = wallLocation.y - random.nextInt(length);
                    } else {
                        corridorX = wallLocation.x + 1;
                        corridorY = wallLocation.y;
                    }
                    break;
                case UP:
                    if (isVertical) {
                        corridorX = wallLocation.x;
                        corridorY = wallLocation.y - length;
                    } else {
                        corridorX = wallLocation.x - random.nextInt(length);
                        corridorY = wallLocation.y - 1;
                    }
                    break;
                default:
                    System.err.println("Something went wrong in createFeature(LevelComponent[][], int, int): direction is NONE");
                    return false;
            }

            feature = new Corridor(corridorX, corridorY, length, isVertical);
        }

        if (feature.getX() <= 0) return false;
        if (feature.getX() + feature.getWidth() >= g.width) return false;
        if (feature.getY() <= 0) return false;
        if (feature.getY() + feature.getHeight() >= g.height) return false;

        for (Feature other : features) {
            if (feature.intersects(other)) {
                // System.err.println(feature + " intersects with " + other + ", direction=" + direction + ", wallLocation=" + wallLocation);
                ;
                return false;
            }
        }

        try {
            for (int x = feature.getX(); x < feature.getX() + feature.getWidth(); x++) {
                for (int y = feature.getY(); y < feature.getY() + feature.getHeight(); y++) {
                    map[x][y] = new Air();
                }
            }
            wallFeature.addEntrance(wallLocation);
            feature.addEntrance(wallLocation);
            map[wallLocation.x][wallLocation.y] = new Air();
        } catch (ArrayIndexOutOfBoundsException e) {
            return false;
        }

        features.add(feature);
        return true;
    }

    public static ReturnValue3<LevelComponent[][], BidirectionalMap<Monster, Point>, HashTable<Point, Point, PointNode[]>> generate(GeneratorSettings g) {
        // 0, initialize
        // 1, fill the map with walls
        // 2, create a room in the center of the map
        // 3, pick a wall of a feature (room, corridor,...)
        // 4, create a new random feature and check if there's enough room to place it
        // 5, if no, go back to 3
        // 6, place the new feature
        // 7, go back to 3 until the dungeon is complete
        // 8, place entry and exit points at random locations
        // 9, place monsters, chests and traps on the map
        // 10, create a shortcut map for faster pathfinding

        // initialize
        LevelComponent[][] map = new LevelComponent[g.width][g.height];
        initialize(g);

        // fill the map with walls
        fill(map, new Wall());

        // create a room in the center of the map
        createFirstRoom(map, g);

        boolean mapFinished = false;
        int failCount = 0;
        while (!mapFinished) {
            // create a new feature and check if it could be placed
            if (!createFeature(map, g)) {
                // if not, try again
                if (++failCount > 500) {
                    System.err.println("can't continue, level is complete");
                    break;
                }
                continue;
            }

            failCount = 0;
            mapFinished = features.size() > g.countFeatures;
        }

        if (g.countEntrances > features.size()) {
            g.countEntrances = features.size() / 2;
        }
        if (g.countExits > features.size()) {
            g.countExits = features.size() / 2;
        }

        // place entrances
        for (int i = 0; i < g.countEntrances; i++) {
            ReturnValue2<Point, Feature> rv2 = getWall();
            Point wallLocation = rv2.value1;
            Feature wallFeature = rv2.value2;
            map[wallLocation.x][wallLocation.y] = new Entrance();
            System.out.println("set entrance " + i + " at " + wallLocation);
            wallFeature.getAdjacentWalls().remove(wallLocation);
        }

        // place exits
        for (int i = 0; i < g.countExits; i++) {
            ReturnValue2<Point, Feature> rv2 = getWall();
            Point wallLocation = rv2.value1;
            Feature wallFeature = rv2.value2;
            map[wallLocation.x][wallLocation.y] = new Exit();
            System.out.println("set exit " + i + " at " + wallLocation);
            wallFeature.getAdjacentWalls().remove(wallLocation);
        }

        // place chests
        for (int i = 0; i < g.countChests; i++) {
            ReturnValue2<Point, Feature> rv2 = getAir();
            Point airLocation = rv2.value1;
            Feature airFeature = rv2.value2;
            map[airLocation.x][airLocation.y] = new Loot();
            System.out.println("set chest " + i + " at " + airLocation);
            airFeature.getContainedPoints().remove(airLocation);
        }

        // place traps
        for (int i = 0; i < g.countTraps; i++) {
            ReturnValue2<Point, Feature> rv2 = getAir();
            Point airLocation = rv2.value1;
            Feature airFeature = rv2.value2;
            map[airLocation.x][airLocation.y] = new Trap();
            System.out.println("set trap " + i + " at " + airLocation);
            airFeature.getContainedPoints().remove(airLocation);
        }

        // place monsters
        for (int i = 0; i < g.countMonsters; i++) {
            ReturnValue2<Point, Feature> rv2 = getAir();
            Point airLocation = rv2.value1;
            Feature airFeature = rv2.value2;
            monsters.put(new Monster(airLocation.x, airLocation.y, Integer.MAX_VALUE, null), airLocation);
            System.out.println("set monster " + i + " at " + airLocation);
            airFeature.getContainedPoints().remove(airLocation);
        }

        // create a shortcut map for faster pathfinding
        HashTable<Point, Point, PointNode[]> shortcuts = new HashTable<>();
        for (Feature feature : features) {
            Set<Point> entrances = feature.getEntrances();
            for (Point entrance : entrances) {
                for (Point otherEntrance : entrances) {
                    if (entrance == otherEntrance) {
                        continue;
                    }
                    shortcuts.set(entrance, otherEntrance, feature.getPath(entrance, otherEntrance));
                }
            }
        }

        // for testing
        // Terminal t = new Terminal(null);
        // for (int x = 0; x < map.length; x++) {
        // for (int y = 0; y < map[x].length; y++) {
        // LevelComponent cur = map[x][y];
        // t.setBackground(cur.getBackground()).setBackground(cur.getForeground()).print(x, y, cur.getSymbol());
        // }
        // }

        return new ReturnValue3<>(map, monsters, shortcuts);
    }

    public static final class GeneratorSettings {
        // calculated settings
        protected int    roomWidthAdd;
        protected int    roomWidthBounds;
        protected int    roomHeightAdd;
        protected int    roomHeightBounds;
        protected int    corridorLengthAdd;
        protected int    corridorLengthBounds;
        protected double countFeatures;

        // general settings
        public int       width             = 0;
        public int       height            = 0;
        public double    complexity        = 1d;

        // component-specific settings
        public int       maxRoomWidth      = 9;
        public int       minRoomWidth      = 3;
        public int       maxRoomHeight     = 9;
        public int       minRoomHeight     = 3;

        public int       maxCorridorLength = 13;
        public int       minCorridorLength = 3;

        // feature-specific settings
        public int       countEntrances    = 1;
        public int       countExits        = 1;
        public int       countChests       = 1;
        public int       countTraps        = 1;
        public int       countMonsters     = 1;

        // random-specific settings
        public long      seed              = new Random().nextLong();

        protected void validate() {
            if (width < 10) throw new IllegalArgumentException("width can not be < 10, is " + width);
            if (height < 10) throw new IllegalArgumentException("height can not be < 10, is " + height);
            if (complexity <= 0d) throw new IllegalArgumentException("complexity can not be <= 0, is " + complexity);

            if (maxRoomWidth <= 0) throw new IllegalArgumentException("maxRoomWidth can not be <= 0, is " + maxRoomWidth);
            if (minRoomWidth <= 0) throw new IllegalArgumentException("minRoomWidth can not be <= 0, is " + minRoomWidth);
            if (maxRoomWidth < minRoomWidth) throw new IllegalArgumentException("maxRoomWidth has to be >= minRoomWidth");
            if (maxRoomHeight <= 0) throw new IllegalArgumentException("maxRoomHeight can not be <= 0, is " + maxRoomHeight);
            if (minRoomHeight <= 0) throw new IllegalArgumentException("minRoomHeight can not be <= 0, is " + minRoomHeight);
            if (maxRoomHeight < minRoomHeight) throw new IllegalArgumentException("maxRoomHeight has to be >= minRoomHeight");

            if (maxCorridorLength <= 0) throw new IllegalArgumentException("maxCorridorLength can not be <= 0, is " + maxCorridorLength);
            if (minCorridorLength <= 0) throw new IllegalArgumentException("minCorridorLength can not be <= 0, is " + minCorridorLength);
            if (maxCorridorLength < minCorridorLength) throw new IllegalArgumentException("maxCorridorLength has to be >= minCorridorLength");

            if (countEntrances < 1) {
                countEntrances = 1;
            }
            if (countExits < 1) {
                countExits = 1;
            }
            if (countChests < 1) {
                countChests = 1;
            }
            if (countTraps < 0) {
                countTraps = 0;
            }
            if (countMonsters < 0) {
                countMonsters = 0;
            }

            roomWidthAdd = minRoomWidth;
            roomWidthBounds = (int) ((maxRoomWidth - minRoomWidth + 1) * complexity);

            roomHeightAdd = minRoomHeight;
            roomHeightBounds = (int) ((maxRoomHeight - minRoomHeight + 1) * complexity);

            corridorLengthAdd = minCorridorLength;
            corridorLengthBounds = (int) ((maxCorridorLength - minCorridorLength + 1) / complexity);

            countFeatures = (width * height * complexity) / (Math.min(height, width));
        }
    }
}
