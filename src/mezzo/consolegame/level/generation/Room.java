/*
 * LICENSE
 * 
 * This work is licensed under the Creative Commons Attribution-ShareAlike 4.0
 * International License. To view a copy of this license, visit
 * http://creativecommons.org/licenses/by-sa/4.0/.
 */
package mezzo.consolegame.level.generation;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import mezzo.consolegame.Level.Point;
import mezzo.consolegame.Level.PointNode;
import mezzo.util.BidirectionalMap;
import mezzo.util.pathfinding.astar.AStar;
import mezzo.util.pathfinding.astar.LocationNode;

/**
 * <code>Room</code>
 * 
 * @author Felix Fr�hlich <felix.froehlich@convey.de>
 */
public class Room implements Feature {
    private final Set<Point>                         walls;
    private final Set<Point>                         points;
    private final BidirectionalMap<Point, PointNode> entrances;
    private final PointNode[][]                      nodeMap;

    public final int                                 x;
    public final int                                 y;
    public final int                                 width;
    public final int                                 height;

    public Room(int x, int y, int width, int height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;

        walls = new HashSet<>();
        for (y = this.y; y < this.y + height; y++) {
            walls.add(new Point(this.x - 1, y));
            walls.add(new Point(this.x + width, y));
        }
        for (x = this.x; x < this.x + width; x++) {
            walls.add(new Point(x, this.y - 1));
            walls.add(new Point(x, this.y + height));
        }

        points = new HashSet<>();
        for (x = this.x; x < this.x + width; x++) {
            for (y = this.y; y < this.y + height; y++) {
                points.add(new Point(x, y));
            }
        }

        entrances = new BidirectionalMap<>();

        nodeMap = new PointNode[width][height];
        for (Point p : points) {
            nodeMap[p.x - this.x][p.y - this.y] = new PointNode(p);
        }
        for (x = 0; x < nodeMap.length; x++) {
            for (y = 0; y < nodeMap[x].length; y++) {
                if (x > 0 && points.contains(new Point(this.x + x - 1, this.y + y))) {
                    nodeMap[x][y].setDistance(nodeMap[x - 1][y], 1d);
                }
                if (x < nodeMap.length - 1 && points.contains(new Point(this.x + x + 1, this.y + y))) {
                    nodeMap[x][y].setDistance(nodeMap[x + 1][y], 1d);
                }
                if (y > 0 && points.contains(new Point(this.x + x, this.y + y - 1))) {
                    nodeMap[x][y].setDistance(nodeMap[x][y - 1], 1d);
                }
                if (y < nodeMap[x].length - 1 && points.contains(new Point(this.x + x, this.y + y + 1))) {
                    nodeMap[x][y].setDistance(nodeMap[x][y + 1], 1d);
                }
            }
        }
    }

    @Override
    public void addEntrance(Point point) {
        if (!walls.contains(point)) throw new IllegalArgumentException(point + " is not a wall of " + this);

        PointNode p = new PointNode(point);
        if (!entrances.containsKey(point)) {
            if (point.y >= y && point.y < y + height) {
                if (point.x == x - 1) {
                    // left
                    p.setDistance(nodeMap[0][point.y - y], 1d);
                    nodeMap[0][point.y - y].setDistance(p, 1d);
                } else if (point.x == x + width) {
                    // right
                    p.setDistance(nodeMap[width - 1][point.y - y], 1d);
                    nodeMap[width - 1][point.y - y].setDistance(p, 1d);
                }
            } else if (point.x >= x && point.x < x + width) {
                if (point.y == y - 1) {
                    // up
                    p.setDistance(nodeMap[point.x - x][0], 1d);
                    nodeMap[point.x - x][0].setDistance(p, 1d);
                } else if (point.y == y + height) {
                    // down
                    p.setDistance(nodeMap[point.x - x][height - 1], 1d);
                    nodeMap[point.x - x][height - 1].setDistance(p, 1d);
                }
            }
            entrances.put(point, p);
            walls.remove(point);
        }
    }

    @Override
    public PointNode[] getPath(Point from, Point to) {
        if (!points.contains(from) && !entrances.containsKey(from)) throw new IllegalArgumentException(from + " is not contained in " + this);
        if (!points.contains(to) && !entrances.containsKey(to)) throw new IllegalArgumentException(to + " is not contained in " + this);

        PointNode fromNode = null;
        PointNode toNode = null;

        if (points.contains(from)) {
            fromNode = nodeMap[from.x - x][from.y - y];
        } else {
            fromNode = entrances.get(from);
        }

        if (points.contains(to)) {
            toNode = nodeMap[to.x - x][to.y - y];
        } else {
            toNode = entrances.get(to);
        }

        fromNode.print();

        LocationNode[] l = AStar.calculate(fromNode, toNode);
        return Arrays.copyOf(l, l.length, PointNode[].class);
    }

    @Override
    public Set<Point> getAdjacentWalls() {
        return walls;
    }

    @Override
    public Set<Point> getContainedPoints() {
        return points;
    }

    @Override
    public Set<Point> getEntrances() {
        return entrances.keySet();
    }

    @Override
    public int getX() {
        return x;
    }

    @Override
    public int getY() {
        return y;
    }

    @Override
    public int getWidth() {
        return width;
    }

    @Override
    public int getHeight() {
        return height;
    }

    @Override
    public String toString() {
        return "Room{x=" + x + ",y=" + y + ",width=" + width + ",height=" + height + "}";
    }
}
