/*
 * LICENSE
 * 
 * This work is licensed under the Creative Commons Attribution-ShareAlike 4.0
 * International License. To view a copy of this license, visit
 * http://creativecommons.org/licenses/by-sa/4.0/.
 */
package mezzo.consolegame.level;

import mezzo.consolegame.Level;

import com.googlecode.lanterna.terminal.Terminal.Color;

/**
 * <code>Air</code>
 * 
 * @author Felix Fr�hlich <felix.froehlich@convey.de>
 */
public class Air implements LevelComponent {
    public static final char  SYMBOL     = ' ';
    public static final Color FOREGROUND = Color.DEFAULT;
    public static final Color BACKGROUND = Color.DEFAULT;

    @Override
    public char getSymbol() {
        return SYMBOL;
    }

    @Override
    public boolean canMoveOn() {
        return true;
    }

    @Override
    public boolean canMove() {
        return false;
    }

    @Override
    public int getType() {
        return Level.AIR;
    }
}
