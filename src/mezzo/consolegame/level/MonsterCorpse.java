/*
 * LICENSE
 * 
 * This work is licensed under the Creative Commons Attribution-ShareAlike 4.0
 * International License. To view a copy of this license, visit
 * http://creativecommons.org/licenses/by-sa/4.0/.
 */
package mezzo.consolegame.level;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import mezzo.consolegame.Level;
import mezzo.consolegame.entity.Monster;
import mezzo.consolegame.item.Item;
import mezzo.consolegame.item.Meat;

import com.googlecode.lanterna.terminal.Terminal.Color;

/**
 * <code>MonsterCorpse</code>
 * 
 * @author Felix Fr�hlich <felix.froehlich@convey.de>
 */
public class MonsterCorpse extends Loot implements LevelComponent {
    public static final char  SYMBOL     = Monster.SYMBOL;
    public static final Color FOREGROUND = Color.MAGENTA;
    public static final Color BACKGROUND = Color.DEFAULT;

    @Override
    public char getSymbol() {
        return SYMBOL;
    }

    @Override
    public boolean canMoveOn() {
        return true;
    }

    @Override
    public boolean canMove() {
        return false;
    }

    @Override
    public int getType() {
        return Level.MONSTER_CORPSE;
    }

    @Override
    public List<? extends Item> getContents() {
        int count = new Random().nextInt(3);
        List<Item> items = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            items.add(new Meat());
        }
        return items;
    }
}
