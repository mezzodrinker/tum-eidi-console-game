/*
 * LICENSE
 * 
 * This work is licensed under the Creative Commons Attribution-ShareAlike 4.0
 * International License. To view a copy of this license, visit
 * http://creativecommons.org/licenses/by-sa/4.0/.
 */
package mezzo.consolegame.level;

import java.util.Random;

import mezzo.consolegame.Level;

import com.googlecode.lanterna.terminal.Terminal.Color;

/**
 * <code>StaticObstacle</code>
 * 
 * @author Felix Fr�hlich <felix.froehlich@convey.de>
 */
public class Trap implements LevelComponent {
    private final int         damage;

    public static final char  SYMBOL     = 'x';      // '\u2620';
    public static final Color FOREGROUND = Color.RED;

    {
        Random r = new Random();
        damage = r.nextInt(3);
    }

    @Override
    public Color getForeground() {
        return FOREGROUND;
    }

    @Override
    public char getSymbol() {
        return SYMBOL;
    }

    @Override
    public boolean canMoveOn() {
        return true;
    }

    @Override
    public boolean canMove() {
        return false;
    }

    @Override
    public int getType() {
        return Level.TRAP;
    }

    public boolean dealsDamage() {
        return damage != 0;
    }

    public int getDamage() {
        return damage;
    }
}
