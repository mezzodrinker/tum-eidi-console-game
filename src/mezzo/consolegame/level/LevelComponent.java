/*
 * LICENSE
 * 
 * This work is licensed under the Creative Commons Attribution-ShareAlike 4.0
 * International License. To view a copy of this license, visit
 * http://creativecommons.org/licenses/by-sa/4.0/.
 */
package mezzo.consolegame.level;

import com.googlecode.lanterna.terminal.Terminal.Color;

/**
 * <code>LevelComponent</code>
 * 
 * @author Felix Fr�hlich <felix.froehlich@convey.de>
 */
public interface LevelComponent {
    public static final char  SYMBOL     = ' ';
    public static final Color FOREGROUND = Color.DEFAULT;

    public default Color getForeground() {
        return Color.DEFAULT;
    }

    public default Color getBackground() {
        return Color.DEFAULT;
    }

    public default boolean hasChanged() {
        return false;
    }

    public default boolean rearm() {
        return false;
    }

    public default void forceUpdate() {}

    public char getSymbol();

    public boolean canMoveOn();

    public boolean canMove();

    public int getType();
}
