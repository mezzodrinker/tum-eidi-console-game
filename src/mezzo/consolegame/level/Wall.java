/*
 * LICENSE
 * 
 * This work is licensed under the Creative Commons Attribution-ShareAlike 4.0
 * International License. To view a copy of this license, visit
 * http://creativecommons.org/licenses/by-sa/4.0/.
 */
package mezzo.consolegame.level;

import mezzo.consolegame.Level;

import com.googlecode.lanterna.terminal.Terminal.Color;

/**
 * <code>Wall</code>
 * 
 * @author Felix Fr�hlich <felix.froehlich@convey.de>
 */
public class Wall implements LevelComponent {
    public static final char  SYMBOL     = '\u2592';
    public static final Color FOREGROUND = Color.WHITE;

    @Override
    public Color getForeground() {
        return FOREGROUND;
    }

    @Override
    public char getSymbol() {
        return SYMBOL;
    }

    @Override
    public boolean canMoveOn() {
        return false;
    }

    @Override
    public boolean canMove() {
        return false;
    }

    @Override
    public int getType() {
        return Level.WALL;
    }
}
