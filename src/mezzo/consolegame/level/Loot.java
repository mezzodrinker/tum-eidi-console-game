/*
 * LICENSE
 * 
 * This work is licensed under the Creative Commons Attribution-ShareAlike 4.0
 * International License. To view a copy of this license, visit
 * http://creativecommons.org/licenses/by-sa/4.0/.
 */
package mezzo.consolegame.level;

import java.util.Arrays;
import java.util.List;

import mezzo.consolegame.Level;
import mezzo.consolegame.item.Item;
import mezzo.consolegame.item.Key;

import com.googlecode.lanterna.terminal.Terminal.Color;

/**
 * <code>Loot</code>
 * 
 * @author Felix Fr�hlich <felix.froehlich@convey.de>
 */
public class Loot implements LevelComponent {
    public static final char  SYMBOL     = '\u25a0';
    public static final Color FOREGROUND = Color.YELLOW;

    @Override
    public Color getForeground() {
        return FOREGROUND;
    }

    @Override
    public char getSymbol() {
        return SYMBOL;
    }

    @Override
    public boolean canMoveOn() {
        return true;
    }

    @Override
    public boolean canMove() {
        return false;
    }

    @Override
    public int getType() {
        return Level.LOOT;
    }

    public List<? extends Item> getContents() {
        return Arrays.asList(new Key());
    }
}
