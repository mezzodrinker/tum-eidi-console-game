/*
 * LICENSE
 * 
 * This work is licensed under the Creative Commons Attribution-ShareAlike 4.0
 * International License. To view a copy of this license, visit
 * http://creativecommons.org/licenses/by-sa/4.0/.
 */
package mezzo.consolegame.listener;

import com.googlecode.lanterna.input.Key;

/**
 * <code>KeyListener</code>
 * 
 * @author Felix Fr�hlich <felix.froehlich@convey.de>
 */
public interface KeyListener {
    public void onKeyPressed(KeyPressedEvent e);

    public static class KeyPressedEvent {
        private boolean  cancelled = false;
        public final Key key;

        public KeyPressedEvent(Key key) {
            this.key = key;
        }

        public Key getKey() {
            return key;
        }

        public boolean isCancelled() {
            return cancelled;
        }

        public void setCancelled(boolean cancelled) {
            this.cancelled = cancelled;
        }
    }
}
