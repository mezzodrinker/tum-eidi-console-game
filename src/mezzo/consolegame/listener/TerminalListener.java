/*
 * LICENSE
 * 
 * This work is licensed under the Creative Commons Attribution-ShareAlike 4.0
 * International License. To view a copy of this license, visit
 * http://creativecommons.org/licenses/by-sa/4.0/.
 */
package mezzo.consolegame.listener;

import java.util.HashSet;
import java.util.Set;

import mezzo.consolegame.gui.Terminal;
import mezzo.consolegame.listener.KeyListener.KeyPressedEvent;

import com.googlecode.lanterna.input.Key;

/**
 * <code>TerminalListener</code>
 * 
 * @author Felix Fr�hlich <felix.froehlich@convey.de>
 */
public class TerminalListener {
    private Set<KeyListener> keyListeners = new HashSet<KeyListener>();
    private final Terminal   terminal;

    public TerminalListener(Terminal terminal) {
        this.terminal = terminal;
    }

    public synchronized void work() {
        Key input = terminal.readInput();
        if (input != null) {
            fireKeyPressedEvent(new KeyPressedEvent(input));
        }
    }

    public void addKeyListener(KeyListener listener) {
        if (!keyListeners.contains(listener)) {
            keyListeners.add(listener);
        }
    }

    public boolean removeKeyListener(KeyListener listener) {
        return keyListeners.remove(listener);
    }

    public void fireKeyPressedEvent(KeyPressedEvent e) {
        // System.out.println("firing key pressed event for key " + e.getKey().getCharacter() + " (" + e.getKey().getKind() + ")");
        for (KeyListener listener : keyListeners) {
            if (e.isCancelled()) {
                break;
            }
            listener.onKeyPressed(e);
        }
    }
}
