/*
 * LICENSE
 * 
 * This work is licensed under the Creative Commons Attribution-ShareAlike 4.0
 * International License. To view a copy of this license, visit
 * http://creativecommons.org/licenses/by-sa/4.0/.
 */
package mezzo.consolegame.listener;

import mezzo.consolegame.event.EntityCollisionEvent;
import mezzo.consolegame.event.EntityDeathEvent;
import mezzo.consolegame.event.EntityHealedEvent;
import mezzo.consolegame.event.EntityHitEvent;
import mezzo.consolegame.event.EntityMovementEvent;
import mezzo.consolegame.event.EntityStepOnTrapEvent;

/**
 * <code>EntityListener</code>
 * 
 * @author Felix Fr�hlich <felix.froehlich@convey.de>
 */
public interface EntityListener {
    public void onEntityCollision(EntityCollisionEvent e);

    public void onEntityMovement(EntityMovementEvent e);

    public void onEntityHit(EntityHitEvent e);

    public void onEntityHealed(EntityHealedEvent e);

    public void onEntityStepOnTrap(EntityStepOnTrapEvent e);

    public void onEntityDeath(EntityDeathEvent e);
}
