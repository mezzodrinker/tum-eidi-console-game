/*
 * LICENSE
 * 
 * This work is licensed under the Creative Commons Attribution-ShareAlike 4.0
 * International License. To view a copy of this license, visit
 * http://creativecommons.org/licenses/by-sa/4.0/.
 */
package mezzo.consolegame.listener;

import mezzo.consolegame.gui.ConsoleFrame;
import mezzo.consolegame.gui.ConsoleFrame.State;

import com.googlecode.lanterna.input.Key.Kind;

/**
 * <code>MainMenuListener</code>
 * 
 * @author Felix Fr�hlich <felix.froehlich@convey.de>
 */
public class MainMenuListener implements KeyListener {
    private boolean            inHelpMenu = false;
    private final ConsoleFrame consoleFrame;

    public MainMenuListener(ConsoleFrame consoleFrame) {
        this.consoleFrame = consoleFrame;
    }

    @Override
    public void onKeyPressed(KeyPressedEvent e) {
        if (consoleFrame.currentState() == State.IN_GAME || consoleFrame.currentState() == State.IN_GAME_MENU || consoleFrame.currentState() == State.IN_GAME_MENU_HELP) return;
        if (e.getKey().getKind() == Kind.Escape) {
            if (!inHelpMenu) {
                consoleFrame.getGame().exit();
                return;
            }
            inHelpMenu = false;
            consoleFrame.openMainMenu();
        }
        if (e.getKey().getCharacter() == 'l' && consoleFrame.currentState() == State.MAIN_MENU) {
            inHelpMenu = false;
            consoleFrame.loadLevel();
            consoleFrame.openInGame();
        }
        if (e.getKey().getCharacter() == 'n' && consoleFrame.currentState() == State.MAIN_MENU) {
            inHelpMenu = false;
            consoleFrame.newLevel();
            consoleFrame.openInGame();
        }
        if (e.getKey().getCharacter() == 'h' && consoleFrame.currentState() == State.MAIN_MENU) {
            inHelpMenu = true;
            consoleFrame.openHelpMainMenu();
        }
    }
}
