/*
 * LICENSE
 * 
 * This work is licensed under the Creative Commons Attribution-ShareAlike 4.0
 * International License. To view a copy of this license, visit
 * http://creativecommons.org/licenses/by-sa/4.0/.
 */
package mezzo.consolegame.listener;

import mezzo.consolegame.event.PlayerDeathEvent;
import mezzo.consolegame.event.PlayerLootCollectEvent;
import mezzo.consolegame.event.PlayerReachedExitEvent;

/**
 * <code>PlayerListener</code>
 * 
 * @author Felix Fr�hlich <felix.froehlich@convey.de>
 */
public interface PlayerListener extends EntityListener {
    public void onPlayerLootCollect(PlayerLootCollectEvent e);

    public void onPlayerReachedExit(PlayerReachedExitEvent e);

    public void onPlayerDeath(PlayerDeathEvent e);
}
