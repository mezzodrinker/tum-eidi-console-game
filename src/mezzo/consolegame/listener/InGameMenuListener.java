/*
 * LICENSE
 * 
 * This work is licensed under the Creative Commons Attribution-ShareAlike 4.0
 * International License. To view a copy of this license, visit
 * http://creativecommons.org/licenses/by-sa/4.0/.
 */
package mezzo.consolegame.listener;

import mezzo.consolegame.gui.ConsoleFrame;
import mezzo.consolegame.gui.ConsoleFrame.State;

import com.googlecode.lanterna.input.Key.Kind;

/**
 * <code>MainMenu</code>
 * 
 * @author Felix Fr�hlich <felix.froehlich@convey.de>
 */
public class InGameMenuListener implements KeyListener {
    private final ConsoleFrame consoleFrame;

    public InGameMenuListener(ConsoleFrame consoleFrame) {
        this.consoleFrame = consoleFrame;
    }

    @Override
    public void onKeyPressed(KeyPressedEvent e) {
        if (consoleFrame.currentState() == State.MAIN_MENU || consoleFrame.currentState() == State.MAIN_MENU_HELP) return;

        if (e.getKey().getKind() == Kind.Escape) {
            if (consoleFrame.currentState() == State.IN_GAME_MENU) {
                consoleFrame.openInGame();
                return;
            }
            consoleFrame.openInGameMenu();
            return;
        }
        if (consoleFrame.currentState() == State.IN_GAME_MENU) {
            if (e.getKey().getCharacter() == 'm') {
                consoleFrame.save();
                consoleFrame.openMainMenu();
                return;
            }
            if (e.getKey().getCharacter() == 'h') {
                consoleFrame.openHelpMenu();
                return;
            }
            if (e.getKey().getCharacter() == 'n') {
                consoleFrame.newLevel();
                consoleFrame.openInGame();
                return;
            }
            if (e.getKey().getCharacter() == 'l') {
                consoleFrame.loadLevel();
                consoleFrame.openInGame();
                return;
            }
            if (e.getKey().getCharacter() == 's') {
                consoleFrame.save();
                consoleFrame.openMainMenu();
                return;
            }
            if (e.getKey().getCharacter() == 'q') {
                consoleFrame.openMainMenu();
                return;
            }
        }
    }
}
