/*
 * LICENSE
 * 
 * This work is licensed under the Creative Commons Attribution-ShareAlike 4.0
 * International License. To view a copy of this license, visit
 * http://creativecommons.org/licenses/by-sa/4.0/.
 */
package mezzo.consolegame.listener;

import mezzo.consolegame.gui.ConsoleFrame;
import mezzo.consolegame.gui.ConsoleFrame.State;

import com.googlecode.lanterna.input.Key.Kind;

/**
 * <code>MovementListener</code>
 * 
 * @author Felix Fr�hlich <felix.froehlich@convey.de>
 */
public class MovementListener implements KeyListener {
    // private long timeSinceLastMovement = 0L;
    private final ConsoleFrame consoleFrame;

    public MovementListener(ConsoleFrame consoleFrame) {
        this.consoleFrame = consoleFrame;
    }

    @Override
    public void onKeyPressed(KeyPressedEvent e) {
        if (consoleFrame.currentState() != State.IN_GAME) return;
        // if (System.currentTimeMillis() - timeSinceLastMovement < 200) return;
        if (e.getKey().getCharacter() == 'a' || e.getKey().getKind() == Kind.ArrowLeft) {
            consoleFrame.getLevel().getPlayer().move(-1, 0);
            // timeSinceLastMovement = System.currentTimeMillis();
            return;
        }
        if (e.getKey().getCharacter() == 's' || e.getKey().getKind() == Kind.ArrowDown) {
            consoleFrame.getLevel().getPlayer().move(0, 1);
            // timeSinceLastMovement = System.currentTimeMillis();
            return;
        }
        if (e.getKey().getCharacter() == 'd' || e.getKey().getKind() == Kind.ArrowRight) {
            consoleFrame.getLevel().getPlayer().move(1, 0);
            // timeSinceLastMovement = System.currentTimeMillis();
            return;
        }
        if (e.getKey().getCharacter() == 'w' || e.getKey().getKind() == Kind.ArrowUp) {
            consoleFrame.getLevel().getPlayer().move(0, -1);
            // timeSinceLastMovement = System.currentTimeMillis();
            return;
        }
        if (e.getKey().getCharacter() == '0') {
            consoleFrame.movement();
            return;
        }
        if (e.getKey().getCharacter() == 'i') {
            consoleFrame.getLevel().getPlayer().toggleInvincible();
            return;
        }
        if (e.getKey().getCharacter() == 'c') {
            consoleFrame.getLevel().getPlayer().cookMeat();
            return;
        }
        if (e.getKey().getCharacter() == 'e') {
            consoleFrame.getLevel().getPlayer().eatMeat();
        }
    }
}
