/*
 * LICENSE
 * 
 * This work is licensed under the Creative Commons Attribution-ShareAlike 4.0
 * International License. To view a copy of this license, visit
 * http://creativecommons.org/licenses/by-sa/4.0/.
 */
package mezzo.consolegame.gui;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Timer;
import java.util.TimerTask;

import mezzo.consolegame.ConsoleGame;
import mezzo.consolegame.Level;
import mezzo.consolegame.Level.Point;
import mezzo.consolegame.entity.Entity;
import mezzo.consolegame.entity.Monster;
import mezzo.consolegame.entity.Player;
import mezzo.consolegame.event.handler.MonsterHandler;
import mezzo.consolegame.event.handler.PlayerHandler;
import mezzo.consolegame.item.Key;
import mezzo.consolegame.level.Air;
import mezzo.consolegame.level.Entrance;
import mezzo.consolegame.level.Exit;
import mezzo.consolegame.level.LevelComponent;
import mezzo.consolegame.level.Loot;
import mezzo.consolegame.level.Trap;
import mezzo.consolegame.level.Wall;
import mezzo.consolegame.listener.InGameMenuListener;
import mezzo.consolegame.listener.MainMenuListener;
import mezzo.consolegame.listener.MovementListener;
import mezzo.consolegame.listener.TerminalListener;
import mezzo.util.BidirectionalMap;

import com.googlecode.lanterna.terminal.Terminal.Color;

/**
 * <code>GUI</code>
 * 
 * @author Felix Fr�hlich <felix.froehlich@convey.de>
 */
public class ConsoleFrame {
    private boolean                          renderedMenu         = false;
    private boolean                          renderedMenuHelp     = false;
    private boolean                          renderedMainMenu     = false;
    private State                            currentState         = State.MAIN_MENU;
    private State                            oldState             = currentState;
    private Point                            oldPlayerLocation    = new Point(-1, -1);
    private LevelComponent                   oldPlayerComponent   = new Air();
    private HashMap<Monster, Point>          oldMonsterLocations  = new HashMap<>();
    private HashMap<Monster, LevelComponent> oldMonsterComponents = new HashMap<>();
    private Terminal                         terminal             = null;
    private TerminalSection                  levelSection         = null;
    private TerminalListener                 terminalListener     = null;
    private Level                            level                = null;
    private Timer                            gameLoop             = null;
    private int                              oldLevelBaseX        = 0;
    private int                              oldLevelBaseY        = 0;
    private int                              levelOffsetX         = 1;
    private int                              levelOffsetY         = 3;
    private int                              levelBaseX           = 0;
    private int                              levelBaseY           = 0;
    private final ConsoleGame                game;

    private static final int                 FPS                  = 30;

    public ConsoleFrame(ConsoleGame game) throws IOException {
        this.game = game;

        // initialize the terminal
        terminal = new Terminal(game);
        levelSection = terminal.createSection(1, 3, terminal.getWidth() - 1, terminal.getHeight() - 4);

        // initialize the terminal listener
        terminalListener = new TerminalListener(terminal);
        terminalListener.addKeyListener(new InGameMenuListener(this));
        terminalListener.addKeyListener(new MainMenuListener(this));
        terminalListener.addKeyListener(new MovementListener(this));

        // initialize the level
        level = null;

        // initialize and start the timer
        gameLoop = new Timer("game loop");
        gameLoop.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                terminalListener.work();
                render();
            }
        }, 0L, 1000L / FPS);
    }

    private void helpHelper(int x, int y, Class<?> c, String description) {
        try {
            terminal.createSection(x, y, 1, 1).setForeground((Color) c.getDeclaredField("FOREGROUND").get(null)).print(0, 0, c.getDeclaredField("SYMBOL").getChar(null));
            terminal.createSection(x + 2, y, description.length(), 1).setForeground(Color.DEFAULT).print(description, Alignment.CENTER);
        } catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException | SecurityException e) {
            e.printStackTrace();
        }
    }

    private void reprintLevel() {
        if (level != null) {
            for (Entry<Entity, Point> entry : level.getEntities().entrySet()) {
                entry.getKey().forceReprint();
            }
            try {
                level.reprint();
            } catch (ArrayIndexOutOfBoundsException e) {
                // TODO find out how this happens!
                // e.printStackTrace();
            }
        }
    }

    private void levelHelper() {
        level.getPlayer().addPlayerListener(new PlayerHandler(this));
        oldMonsterLocations.clear();
        oldMonsterComponents.clear();
        for (Monster monster : level.getMonsters().keySet()) {
            monster.addEntityListener(new MonsterHandler(this));
            oldMonsterLocations.put(monster, new Point(-1, -1));
            oldMonsterComponents.put(monster, level.get(monster.getPosition().x, monster.getPosition().y));
        }
        oldPlayerLocation = new Point(-1, -1);
        oldPlayerComponent = level.get(level.getPlayer().getPosition().x, level.getPlayer().getPosition().y);
    }

    private void loading() {
        terminal.clearScreen();
        terminal.createSection(0, (terminal.getHeight() - 1) / 2, terminal.getWidth(), 1).print("Loading...", Alignment.CENTER);
    }

    private void clearInputBuffer() {
        while (terminal.readInput() != null) {
            ;
        }
    }

    public State currentState() {
        return currentState;
    }

    public ConsoleGame getGame() {
        return game;
    }

    public Terminal getTerminal() {
        return terminal;
    }

    public Level getLevel() {
        return level;
    }

    public int getLevelBaseX() {
        return levelBaseX;
    }

    public int getLevelBaseY() {
        return levelBaseY;
    }

    public int getLevelOffsetX() {
        return levelOffsetX;
    }

    public int getLevelOffsetY() {
        return levelOffsetY;
    }

    public void loadLevel() {
        loading();
        level = game.loadLevel();
        levelHelper();
    }

    public void newLevel() {
        loading();
        level = game.newLevel();
        levelHelper();
    }

    public void save() {
        game.save(level);
    }

    public synchronized void movement() {
        BidirectionalMap<Monster, Point> copy = new BidirectionalMap<>(level.getMonsters());
        for (Entry<Monster, Point> entry : copy.entrySet()) {
            Monster monster = entry.getKey();
            if (!monster.isAlive()) {
                level.removeEntity(monster);
                level.set(monster.getPosition().x, monster.getPosition().y, Level.MONSTER_CORPSE);
                return;
            }
            Point currentLocation = level.getMonsters().get(monster);
            Point target = null;

            if (currentLocation.distanceTo(level.getPlayer().getPosition()) <= 10d) {
                target = level.getPlayer().getPosition();
            }

            if (target != null) {
                monster.setCurrentPath(level.getShortestPath(currentLocation, target));
            }

            if (level.getMonsters().getKey(monster.getNextPosition()) == null) {
                Point next = monster.nextPosition();
                if (next.distanceTo(monster.getPosition()) > 1) {
                    System.err.println("[ERROR] distance from " + next + " to " + monster.getPosition() + " is " + next.distanceTo(monster.getPosition()) + ", something is off");
                }
                monster.setPosition(next.x, next.y);
                if (monster.isAlive()) {
                    level.getMonsters().put(monster, monster.getPosition());
                }
            }
        }
    }

    public synchronized void render() {
        int width = terminal.getWidth();
        int height = terminal.getHeight();

        if (level != null) {
            levelBaseX = (level.getPlayer().getPosition().x / levelSection.width) * levelSection.width;
            levelBaseY = (level.getPlayer().getPosition().y / levelSection.height) * levelSection.height;
        }

        if (currentState != State.IN_GAME_MENU) {
            renderedMenu = false;
        }
        if (currentState != State.IN_GAME_MENU_HELP && currentState != State.MAIN_MENU_HELP) {
            renderedMenuHelp = false;
        }
        if (currentState != State.MAIN_MENU) {
            renderedMainMenu = false;
        }
        if (level != null && currentState != State.IN_GAME) {
            level.getPlayer().forceUpdate();
            for (Entry<Entity, Point> entry : level.getEntities().entrySet()) {
                entry.getKey().forceReprint();
            }
            oldPlayerLocation = new Point(-1, -1);
        }
        if (oldState != currentState) {
            terminal.clearScreen();
            oldState = currentState;
        }
        if ((oldLevelBaseY != levelBaseY || oldLevelBaseX != levelBaseX) && level != null) {
            System.out.println("[INFO] " + oldLevelBaseX + "|" + oldLevelBaseY + " " + levelBaseX + "|" + levelBaseY + " " + level.getPlayer().getPosition().x + "|"
                    + level.getPlayer().getPosition().y + " " + levelSection.width + "|" + levelSection.height + " " + width + "|" + height);
            oldLevelBaseY = levelBaseY;
            oldLevelBaseX = levelBaseX;
            terminal.clearScreen();
            reprintLevel();
            level.getPlayer().getInventory().forceUpdate();
        }

        switch (currentState) {
            case IN_GAME_MENU: {
                if (renderedMenu) return;
                System.out.println("rendering menu...");
                renderedMenu = true;
                reprintLevel();

                Color background = Color.BLACK;
                Color foreground = Color.WHITE;
                List<String> lines = new ArrayList<String>();
                lines.add("IN-GAME MENU");
                lines.add("");
                lines.add("[M]ain menu");
                lines.add("[H]elp");
                lines.add("[N]ew game");
                lines.add("[L]oad");
                lines.add("[S]ave and quit");
                lines.add("[Q]uit");
                lines.add("");
                lines.add("Press [Esc] to return to the game.");
                terminal.createSection((width - 40) / 2, (height - lines.size()) / 2, 40, lines.size()).setForeground(foreground).setBackground(background).print(lines, Alignment.CENTER);
                break;
            }
            case MAIN_MENU_HELP:
            case IN_GAME_MENU_HELP: {
                if (renderedMenuHelp) return;
                System.out.println("rendering help menu...");
                renderedMenuHelp = true;
                reprintLevel();

                int line = 1;
                List<String> lines = new ArrayList<String>();
                lines.add("HELP");
                lines.add("====");
                lines.add("");
                terminal.createSection((width - 40) / 2, line, 40, lines.size()).print(lines, Alignment.CENTER);
                line += lines.size();
                helpHelper(1, line++, Player.class, "This is you. Yes, you're a circle. Deal with it.");
                helpHelper(1, line++, Wall.class, "This is a wall. You can't walk onto it.");
                helpHelper(1, line++, Entrance.class, "This is an entry point. You might show up here when you start a new game.");
                helpHelper(1, line++, Exit.class, "This is the exit. If you found enough keys and find one, you win!");
                helpHelper(1, line++, Trap.class, "This is a trap. It might or might not hurt you.");
                helpHelper(1, line++, Monster.class, "This is a monster. Get too near and it will hunt you down.");
                helpHelper(1, line++, Loot.class, "This is a loot chest. Take care, some of them might be trapped!");
                lines.clear();
                lines.add("");
                lines.add("CONTROLS");
                lines.add("========");
                lines.add("");
                lines.add("A / Arrow up: go north");
                lines.add("S / Arrow down: go south");
                lines.add("D / Arrow right: go east");
                lines.add("W / Arrow left: go west");
                lines.add("C: cook meat");
                lines.add("E: eat meat");
                lines.add("");
                lines.add("Press [Esc] to return to the menu.");
                terminal.createSection((width - 40) / 2, line, 40, lines.size()).print(lines, Alignment.CENTER);
                line += lines.size();
                break;
            }
            case IN_GAME: {
                if (level.getPlayer().hasChanged()) {
                    level.getPlayer().print();
                    level.getPlayer().rearm();
                }

                Entry<LevelComponent, Point> entry;
                if (level == null) throw new RuntimeException("level is null even though current state is " + currentState);
                Player player = level.getPlayer();

                while ((entry = level.nextUpdate()) != null) {
                    Point l = entry.getValue();
                    LevelComponent c = entry.getKey();
                    if (l.x < levelBaseX || l.x >= levelBaseX + levelSection.width || l.y < levelBaseY || l.y >= levelBaseY + levelSection.height) {
                        continue;
                    }
                    if (l.equals(player.getPosition())) {
                        continue;
                    }
                    levelSection.setForeground(c.getForeground()).setBackground(c.getBackground()).print(l.x - levelBaseX, l.y - levelBaseY, entry.getKey().getSymbol());
                }
                if (player.hasMoved()) {
                    Monster monster = level.getMonsters().getKey(oldPlayerLocation);
                    try {
                        if (monster == null) {
                            if (!oldPlayerLocation.equals(player.getPosition())) {
                                levelSection.setForeground(oldPlayerComponent.getForeground()).setBackground(oldPlayerComponent.getBackground())
                                        .print(oldPlayerLocation.x - levelBaseX, oldPlayerLocation.y - levelBaseY, oldPlayerComponent.getSymbol());
                                // level.reprint(oldPlayerLocation.x, oldPlayerLocation.y);
                            }
                        } else {
                            monster.forceReprint();
                        }
                    } catch (ArrayIndexOutOfBoundsException e) {
                    }
                    oldPlayerLocation = player.getPosition();
                    oldPlayerComponent = level.get(player.getPosition().x, player.getPosition().y);

                    levelSection.setForeground(player.getForeground()).setBackground(player.getBackground())
                            .print(player.getPosition().x - levelBaseX, player.getPosition().y - levelBaseY, player.getSymbol());

                    player.rearmMovement();
                }
                for (Monster monster : level.getMonsters().keySet()) {
                    if (monster.hasMoved()) {
                        LevelComponent oldMonsterComponent = oldMonsterComponents.get(monster);
                        Point oldMonsterLocation = oldMonsterLocations.get(monster);
                        try {
                            if (!oldMonsterLocation.equals(monster.getPosition())) {
                                levelSection.setForeground(oldMonsterComponent.getForeground()).setBackground(oldMonsterComponent.getBackground())
                                        .print(oldMonsterLocation.x - levelBaseX, oldMonsterLocation.y - levelBaseY, oldMonsterComponent.getSymbol());
                                // level.reprint(oldMonsterLocation.x, oldMonsterLocation.y);
                            }
                        } catch (ArrayIndexOutOfBoundsException e) {
                        }
                        oldMonsterLocations.put(monster, monster.getPosition());
                        oldMonsterComponents.put(monster, level.get(monster.getPosition().x, monster.getPosition().y));

                        levelSection.setForeground(monster.getForeground()).setBackground(monster.getBackground())
                                .print(monster.getPosition().x - levelBaseX, monster.getPosition().y - levelBaseY, monster.getSymbol());

                        monster.rearmMovement();
                    }
                }
                break;
            }
            case MAIN_MENU: {
                if (renderedMainMenu) return;
                System.out.println("rendering main menu...");
                renderedMainMenu = true;
                reprintLevel();

                Color foreground = Color.WHITE;
                Color background = Color.BLACK;
                List<String> lines = new ArrayList<String>();
                lines.add("MAIN MENU");
                lines.add("=========");
                lines.add("");
                lines.add("[L]oad");
                lines.add("[N]ew Game");
                lines.add("[H]elp");
                lines.add("");
                lines.add("Press [Esc] to quit.");
                terminal.createSection((width - 40) / 2, (height - lines.size()) / 2, 40, lines.size()).setForeground(foreground).setBackground(background).print(lines, Alignment.CENTER);
                break;
            }
            default:
                break;
        }
    }

    public void openInGameMenu() {
        oldState = currentState;
        currentState = State.IN_GAME_MENU;
    }

    public void openInGame() {
        oldState = currentState;
        currentState = State.IN_GAME;
    }

    public void openHelpMenu() {
        oldState = currentState;
        currentState = State.IN_GAME_MENU_HELP;
    }

    public void openMainMenu() {
        oldState = currentState;
        currentState = State.MAIN_MENU;
    }

    public void openHelpMainMenu() {
        oldState = currentState;
        currentState = State.MAIN_MENU_HELP;
    }

    public void win() {
        level.getPlayer().getInventory().remove(Key.class);
        terminal.clearScreen();
        currentState = State.NONE;
        terminal.createSection(0, (terminal.getHeight() - 1) / 2, terminal.getWidth(), 1).print("You win!", Alignment.CENTER);
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        clearInputBuffer();

        currentState = State.MAIN_MENU;
    }

    public void lose() {
        terminal.clearScreen();
        currentState = State.NONE;
        terminal.createSection(0, 0, terminal.getWidth(), terminal.getHeight()).setBackground(Color.RED).print();
        terminal.createSection(0, (terminal.getHeight() - 1) / 2, terminal.getWidth(), 1).setBackground(Color.RED).setForeground(Color.WHITE).print("You died. It's GAME OVER, man.", Alignment.CENTER);
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        clearInputBuffer();

        currentState = State.MAIN_MENU;
    }

    public static enum State {
        NONE, IN_GAME_MENU, IN_GAME_MENU_HELP, IN_GAME, MAIN_MENU, MAIN_MENU_HELP;
    }
}
