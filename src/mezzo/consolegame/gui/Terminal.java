/*
 * LICENSE
 * 
 * This work is licensed under the Creative Commons Attribution-ShareAlike 4.0
 * International License. To view a copy of this license, visit
 * http://creativecommons.org/licenses/by-sa/4.0/.
 */
package mezzo.consolegame.gui;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.WindowConstants;

import mezzo.consolegame.ConsoleGame;

import com.googlecode.lanterna.TerminalFacade;
import com.googlecode.lanterna.input.Key;
import com.googlecode.lanterna.terminal.Terminal.Color;
import com.googlecode.lanterna.terminal.swing.SwingTerminal;

/**
 * <code>Terminal</code>
 * 
 * @author Felix Fr�hlich <felix.froehlich@convey.de>
 */
public class Terminal extends TerminalSection {
    private final SwingTerminal terminal;

    public Terminal(ConsoleGame game) {
        // initialize the terminal
        terminal = TerminalFacade.createSwingTerminal();
        terminal.setCursorVisible(false);
        terminal.enterPrivateMode();

        // set up TerminalSection attributes
        offsetX = 0;
        offsetY = 0;
        width = terminal.getTerminalSize().getColumns();
        height = terminal.getTerminalSize().getRows();
        super.terminal = terminal;

        // set up the JFrame
        terminal.getJFrame().setTitle("Console Game (\u00A92014, Felix Fr�hlich)");
        terminal.getJFrame().setDefaultCloseOperation(ConsoleGame.isDebugEnabled ? WindowConstants.EXIT_ON_CLOSE : WindowConstants.DO_NOTHING_ON_CLOSE);
        terminal.getJFrame().setResizable(false);
        terminal.getJFrame().addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                if (game != null) {
                    game.exit();
                }
            }
        });
    }

    @Override
    protected void cleanup() {
        terminal.applyForegroundColor(Color.DEFAULT);
        terminal.applyBackgroundColor(Color.DEFAULT);
    }

    protected SwingTerminal getTerminal() {
        return terminal;
    }

    public void clearScreen() {
        terminal.clearScreen();
    }

    public JFrame getJFrame() {
        return terminal.getJFrame();
    }

    public int getWidth() {
        return terminal.getTerminalSize().getColumns();
    }

    public int getHeight() {
        return terminal.getTerminalSize().getRows();
    }

    public Key readInput() {
        return terminal.readInput();
    }

    @Deprecated
    @Override
    public void print(String line, Alignment alignment) {
        super.print(line, alignment);
    }

    @Deprecated
    @Override
    public void print(List<String> lines, Alignment alignment) {
        super.print(lines, alignment);
    }

    public TerminalSection createSection(int x, int y, int width, int height) {
        return new TerminalSection(this, x, y, width, height);
    }
}
