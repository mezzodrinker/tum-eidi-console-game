/*
 * LICENSE
 * 
 * This work is licensed under the Creative Commons Attribution-ShareAlike 4.0
 * International License. To view a copy of this license, visit
 * http://creativecommons.org/licenses/by-sa/4.0/.
 */
package mezzo.consolegame.gui;

/**
 * <code>Alignment</code>
 * 
 * @author Felix Fr�hlich <felix.froehlich@convey.de>
 */
public enum Alignment {
    LEFT, CENTER, RIGHT;
}
