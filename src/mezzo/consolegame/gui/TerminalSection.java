/*
 * LICENSE
 * 
 * This work is licensed under the Creative Commons Attribution-ShareAlike 4.0
 * International License. To view a copy of this license, visit
 * http://creativecommons.org/licenses/by-sa/4.0/.
 */
package mezzo.consolegame.gui;

import java.util.ArrayList;
import java.util.List;

import com.googlecode.lanterna.terminal.Terminal.Color;
import com.googlecode.lanterna.terminal.swing.SwingTerminal;

/**
 * <code>TerminalSection</code>
 * 
 * @author Felix Fr�hlich <felix.froehlich@convey.de>
 */
public class TerminalSection {
    protected Color         foreground = Color.DEFAULT;
    protected Color         background = Color.DEFAULT;
    protected int           offsetX;
    protected int           offsetY;
    protected int           width;
    protected int           height;
    protected SwingTerminal terminal;

    protected TerminalSection() {}

    protected TerminalSection(Terminal parent, int x, int y, int width, int height) {
        terminal = parent.getTerminal();
        offsetX = x;
        offsetY = y;
        this.width = width;
        this.height = height;
    }

    protected void cleanup() {
        terminal.applyForegroundColor(Color.DEFAULT);
        terminal.applyBackgroundColor(Color.DEFAULT);
    }

    public TerminalSection setForeground(Color foreground) {
        this.foreground = foreground;
        return this;
    }

    public TerminalSection setBackground(Color background) {
        this.background = background;
        return this;
    }

    public void print() {
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                print(x, y, ' ');
            }
        }
    }

    public void print(int x, int y, char c) {
        int posX = offsetX + x;
        int posY = offsetY + y;
        if (posX >= offsetX + width) return;
        if (posY >= offsetY + height) return;
        if (posX < offsetX) return;
        if (posY < offsetY) return;
        terminal.moveCursor(posX, posY);
        terminal.applyForegroundColor(foreground);
        terminal.applyBackgroundColor(background);
        terminal.putCharacter(c);
        cleanup();
    }

    public void print(String line, Alignment alignment) {
        List<String> lines = new ArrayList<>();
        int pos = 0;
        do {
            lines.add(line.substring(pos, Math.min(line.length(), (pos += width))));
        } while (pos < line.length());
        print(lines, alignment);
    }

    public void print(List<String> lines, Alignment alignment) {
        for (int y = 0; y < Math.min(height, lines.size()); y++) {
            int linePos = 0;
            String line = null;

            if (y < lines.size()) {
                line = lines.get(y);
            }

            for (int x = 0; x < width; x++) {
                if (line != null && linePos < line.length()) {
                    switch (alignment) {
                        case LEFT: {
                            print(x, y, line.charAt(linePos++));
                            continue;
                        }
                        case CENTER: {
                            if (x >= (width - line.length()) / 2) {
                                print(x, y, line.charAt(linePos++));
                                continue;
                            }
                            break;
                        }
                        case RIGHT: {
                            if (x >= width - line.length()) {
                                print(x, y, line.charAt(linePos++));
                                continue;
                            }
                            break;
                        }
                        default:
                            throw new UnsupportedOperationException();
                    }
                }
                print(x, y, ' ');
            }
        }
    }
}
