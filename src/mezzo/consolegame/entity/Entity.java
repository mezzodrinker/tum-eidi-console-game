/*
 * LICENSE
 * 
 * This work is licensed under the Creative Commons Attribution-ShareAlike 4.0
 * International License. To view a copy of this license, visit
 * http://creativecommons.org/licenses/by-sa/4.0/.
 */
package mezzo.consolegame.entity;

import mezzo.consolegame.Level;
import mezzo.consolegame.Level.Point;
import mezzo.consolegame.event.EntityCollisionEvent;
import mezzo.consolegame.event.EntityDeathEvent;
import mezzo.consolegame.event.EntityHealedEvent;
import mezzo.consolegame.event.EntityHitEvent;
import mezzo.consolegame.event.EntityMovementEvent;
import mezzo.consolegame.event.EntityStepOnTrapEvent;
import mezzo.consolegame.item.Item;
import mezzo.consolegame.level.LevelComponent;
import mezzo.consolegame.listener.EntityListener;

/**
 * <code>Entity</code>
 * 
 * @author Felix Fr�hlich <felix.froehlich@convey.de>
 */
public interface Entity {
    public Level getLevel();

    public Point getPosition();

    public default void setPosition(int x, int y) {
        move(x - getPosition().x, y - getPosition().y);
    }

    public default void setPosition(Point p) {
        setPosition(p.x, p.y);
    }

    public boolean hasMoved();

    public boolean rearmMovement();

    public void forceReprint();

    public void move(int x, int y);

    public int getHealth();

    public default boolean isAlive() {
        return getHealth() > 0;
    }

    public void heal(int amount, Entity source);

    public void heal(int amount, Item source);

    public void damage(int damage, Entity source);

    public void damage(int damage, Item source);

    public void damage(int damage, LevelComponent source);

    public void fireEntityHitEvent(EntityHitEvent e);

    public void fireEntityHealedEvent(EntityHealedEvent e);

    public void fireEntityCollisionEvent(EntityCollisionEvent e);

    public void fireEntityMovementEvent(EntityMovementEvent e);

    public void fireEntityStepOnTrapEvent(EntityStepOnTrapEvent e);

    public void fireEntityDeathEvent(EntityDeathEvent e);

    public void addEntityListener(EntityListener l);

    public boolean removeEntityListener(EntityListener l);
}