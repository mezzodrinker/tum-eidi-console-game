/*
 * LICENSE
 * 
 * This work is licensed under the Creative Commons Attribution-ShareAlike 4.0
 * International License. To view a copy of this license, visit
 * http://creativecommons.org/licenses/by-sa/4.0/.
 */
package mezzo.consolegame.entity;

import java.util.LinkedList;
import java.util.List;

import mezzo.consolegame.Level;
import mezzo.consolegame.Level.Point;
import mezzo.consolegame.Level.PointNode;
import mezzo.consolegame.event.EntityCollisionEvent;
import mezzo.consolegame.event.EntityDeathEvent;
import mezzo.consolegame.event.EntityHealedEvent;
import mezzo.consolegame.event.EntityHitEvent;
import mezzo.consolegame.event.EntityMovementEvent;
import mezzo.consolegame.event.EntityStepOnTrapEvent;
import mezzo.consolegame.item.Item;
import mezzo.consolegame.level.LevelComponent;
import mezzo.consolegame.level.Trap;
import mezzo.consolegame.listener.EntityListener;

import com.googlecode.lanterna.terminal.Terminal.Color;

/**
 * <code>Enemy</code>
 * 
 * @author Felix Fr�hlich <felix.froehlich@convey.de>
 */
public class Monster implements NonPlayerEntity {
    private boolean              changed     = false;
    private boolean              moved       = true;
    private boolean              invincible  = false;
    private int                  x           = 0;
    private int                  y           = 0;
    private int                  health      = 3;
    private int                  pathCounter = 0;
    private PointNode[]          currentPath = null;
    private Level                level       = null;
    private List<EntityListener> listeners   = new LinkedList<EntityListener>();

    public static final char     SYMBOL      = 'o';
    public static final Color    FOREGROUND  = Color.RED;
    public static final Color    BACKGROUND  = Color.DEFAULT;

    public Monster(int x, int y, int health, Level level) {
        this.x = x;
        this.y = y;
        this.health = health;
        this.level = level;
    }

    @Override
    public boolean hasMoved() {
        return moved;
    }

    @Override
    public void forceReprint() {
        moved = true;
    }

    @Override
    public boolean rearmMovement() {
        boolean b = moved;
        moved = false;
        return b;
    }

    public boolean hasChanged() {
        return changed;
    }

    public boolean rearm() {
        boolean b = changed;
        changed = false;
        return b;
    }

    public void forceUpdate() {
        changed = true;
    }

    public Color getForeground() {
        return FOREGROUND;
    }

    public Color getBackground() {
        return BACKGROUND;
    }

    public char getSymbol() {
        return SYMBOL;
    }

    public int getType() {
        return Level.MONSTER;
    }

    @Override
    public Point getPosition() {
        return new Point(x, y);
    }

    @Override
    public void move(int x, int y) {
        Point oldPosition = new Point(this.x, this.y);
        Point newPosition = new Point(this.x + x, this.y + y);

        // handle invalid values
        if (newPosition.x < 0) {
            newPosition = new Point(0, newPosition.y);
        } else if (newPosition.x >= level.getWidth()) {
            newPosition = new Point(level.getWidth() - 1, newPosition.y);
        }

        if (newPosition.y < 0) {
            newPosition = new Point(newPosition.x, 0);
        } else if (newPosition.y >= level.getHeight()) {
            newPosition = new Point(newPosition.x, level.getHeight() - 1);
        }

        LevelComponent component = level.get(newPosition.x, newPosition.y);
        if (!newPosition.equals(oldPosition) && component.canMoveOn()) {
            Entity other;
            if (component instanceof Trap) {
                fireEntityStepOnTrapEvent(new EntityStepOnTrapEvent(this, (Trap) component, newPosition, level));
            } else if ((other = level.getEntities().getKey(newPosition)) != null) {
                fireEntityCollisionEvent(new EntityCollisionEvent(this, other));
                other.fireEntityCollisionEvent(new EntityCollisionEvent(other, this));
                return;
            }

            this.x += x;
            this.y += y;
            moved = true;
            fireEntityMovementEvent(new EntityMovementEvent(this, oldPosition, newPosition, level));
        }
    }

    @Override
    public int getHealth() {
        return health;
    }

    @Override
    public void damage(int damage, Entity source) {
        if (damage < 0) throw new IllegalArgumentException("damage can not be < 0");
        if (invincible) {
            System.out.println(this + " was damaged by " + source + " but " + this + " is invincible");
            return;
        }
        health -= damage;
        if (damage > 0) {
            changed = true;
            fireEntityHitEvent(new EntityHitEvent(this, source, damage));
        }
    }

    @Override
    public void damage(int damage, Item source) {
        if (damage < 0) throw new IllegalArgumentException("damage can not be < 0");
        if (invincible) {
            System.out.println(this + " was damaged by " + source + " but " + this + " is invincible");
            return;
        }
        health -= damage;
        if (damage > 0) {
            changed = true;
            fireEntityHitEvent(new EntityHitEvent(this, source, damage));
        }
    }

    @Override
    public void damage(int damage, LevelComponent source) {
        if (damage < 0) throw new IllegalArgumentException("damage can not be < 0");
        if (invincible) {
            System.out.println(this + " was damaged by " + source + " but " + this + " is invincible");
            return;
        }
        health -= damage;
        if (damage > 0) {
            changed = true;
            fireEntityHitEvent(new EntityHitEvent(this, source, damage));
        }
    }

    @Override
    public void heal(int amount, Entity source) {
        if (amount < 0) throw new IllegalArgumentException("amount can not be < 0");
        health += amount;
        if (amount > 0) {
            changed = true;
            fireEntityHealedEvent(new EntityHealedEvent(this, source, amount));
        }
    }

    @Override
    public void heal(int amount, Item source) {
        if (amount < 0) throw new IllegalArgumentException("amount can not be < 0");
        health += amount;
        if (amount > 0) {
            changed = true;
            fireEntityHealedEvent(new EntityHealedEvent(this, source, amount));
        }
    }

    @Override
    public Level getLevel() {
        return level;
    }

    @Override
    public void fireEntityHitEvent(EntityHitEvent e) {
        System.out.println("firing entity hit event");
        for (EntityListener l : listeners) {
            if (e.isCancelled()) {
                break;
            }
            l.onEntityHit(e);
        }
    }

    @Override
    public void fireEntityHealedEvent(EntityHealedEvent e) {
        System.out.println("firing entity healed event");
        for (EntityListener l : listeners) {
            if (e.isCancelled()) {
                break;
            }
            l.onEntityHealed(e);
        }
    }

    @Override
    public void fireEntityCollisionEvent(EntityCollisionEvent e) {
        System.out.println("firing entity collision event");
        for (EntityListener l : listeners) {
            if (e.isCancelled()) {
                break;
            }
            l.onEntityCollision(e);
        }
    }

    @Override
    public void fireEntityMovementEvent(EntityMovementEvent e) {
        System.out.println("firing entity movement event: " + e.getFrom().toString() + " -> " + e.getTo().toString());
        for (EntityListener l : listeners) {
            if (e.isCancelled()) {
                break;
            }
            l.onEntityMovement(e);
        }
    }

    @Override
    public void fireEntityStepOnTrapEvent(EntityStepOnTrapEvent e) {
        System.out.println("firing entity step on trap event");
        for (EntityListener l : listeners) {
            if (e.isCancelled()) {
                break;
            }
            l.onEntityStepOnTrap(e);
        }
    }

    @Override
    public void fireEntityDeathEvent(EntityDeathEvent e) {
        System.out.println("firing entity death event");
        for (EntityListener l : listeners) {
            if (e.isCancelled()) {
                break;
            }
            l.onEntityDeath(e);
        }
    }

    @Override
    public void addEntityListener(EntityListener l) {
        if (!listeners.contains(l)) {
            listeners.add(l);
        }
    }

    @Override
    public boolean removeEntityListener(EntityListener l) {
        return listeners.remove(l);
    }

    @Override
    public PointNode[] getCurrentPath() {
        return currentPath;
    }

    @Override
    public void setCurrentPath(PointNode[] path) {
        currentPath = path;
        pathCounter = 0;
    }

    @Override
    public Point nextPosition() {
        if (currentPath == null) return getPosition();
        if (pathCounter + 1 < currentPath.length) return currentPath[++pathCounter].toPoint();
        return currentPath[pathCounter].toPoint();
    }

    @Override
    public Point getNextPosition() {
        if (currentPath == null) return getPosition();
        if (pathCounter + 1 < currentPath.length) return currentPath[pathCounter + 1].toPoint();
        return currentPath[pathCounter].toPoint();
    }

    @Override
    public boolean targetReached() {
        if (currentPath == null) return true;
        return pathCounter == currentPath.length - 1;
    }
}
