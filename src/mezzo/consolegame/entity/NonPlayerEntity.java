/*
 * LICENSE
 * 
 * This work is licensed under the Creative Commons Attribution-ShareAlike 4.0
 * International License. To view a copy of this license, visit
 * http://creativecommons.org/licenses/by-sa/4.0/.
 */
package mezzo.consolegame.entity;

import mezzo.consolegame.Level.Point;
import mezzo.consolegame.Level.PointNode;

/**
 * <code>NonPlayerEntity</code>
 * 
 * @author Felix Fr�hlich <felix.froehlich@convey.de>
 */
public interface NonPlayerEntity extends Entity {
    public PointNode[] getCurrentPath();

    public void setCurrentPath(PointNode[] path);

    public Point nextPosition();

    public Point getNextPosition();

    public boolean targetReached();
}
