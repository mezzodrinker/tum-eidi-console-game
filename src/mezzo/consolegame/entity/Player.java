/*
 * LICENSE
 * 
 * This work is licensed under the Creative Commons Attribution-ShareAlike 4.0
 * International License. To view a copy of this license, visit
 * http://creativecommons.org/licenses/by-sa/4.0/.
 */
package mezzo.consolegame.entity;

import java.util.LinkedList;
import java.util.List;

import mezzo.consolegame.Inventory;
import mezzo.consolegame.Level;
import mezzo.consolegame.Level.Point;
import mezzo.consolegame.event.EntityCollisionEvent;
import mezzo.consolegame.event.EntityDeathEvent;
import mezzo.consolegame.event.EntityHealedEvent;
import mezzo.consolegame.event.EntityHitEvent;
import mezzo.consolegame.event.EntityMovementEvent;
import mezzo.consolegame.event.EntityStepOnTrapEvent;
import mezzo.consolegame.event.PlayerDeathEvent;
import mezzo.consolegame.event.PlayerLootCollectEvent;
import mezzo.consolegame.event.PlayerReachedExitEvent;
import mezzo.consolegame.gui.Alignment;
import mezzo.consolegame.gui.Terminal;
import mezzo.consolegame.gui.TerminalSection;
import mezzo.consolegame.item.CookedMeat;
import mezzo.consolegame.item.Item;
import mezzo.consolegame.item.Meat;
import mezzo.consolegame.level.Exit;
import mezzo.consolegame.level.LevelComponent;
import mezzo.consolegame.level.Loot;
import mezzo.consolegame.level.Trap;
import mezzo.consolegame.listener.EntityListener;
import mezzo.consolegame.listener.PlayerListener;

import com.googlecode.lanterna.terminal.Terminal.Color;

/**
 * <code>Player</code>
 * 
 * @author Felix Fr�hlich <felix.froehlich@convey.de>
 */
public class Player implements Entity {
    private boolean               changed         = false;
    private boolean               moved           = true;
    private boolean               invincible      = false;
    private int                   x               = 0;
    private int                   y               = 0;
    private int                   health          = 3;
    private int                   kills           = 0;
    private Inventory             inventory       = null;
    private Level                 level           = null;
    private List<EntityListener>  entityListeners = new LinkedList<EntityListener>();
    private List<PlayerListener>  listeners       = new LinkedList<PlayerListener>();
    private final TerminalSection section;

    public static final char      SYMBOL          = 'o';
    public static final Color     FOREGROUND      = Color.GREEN;
    public static final Color     BACKGROUND      = Color.DEFAULT;

    public Player(int x, int y, int health, Level level, Terminal terminal) {
        this.x = x;
        this.y = y;
        this.health = health;
        this.level = level;
        section = terminal.createSection(0, 0, terminal.getWidth(), 1).setForeground(Color.BLACK).setBackground(Color.WHITE);
        inventory = new Inventory(terminal);
    }

    public Inventory getInventory() {
        return inventory;
    }

    public void addKills(int kills) {
        this.kills += kills;
        changed = true;
    }

    public void cookMeat() {
        if (inventory.getItemCount(Meat.class) > 0) {
            inventory.remove(Meat.class);
            inventory.add(new CookedMeat());
        }
    }

    public void eatMeat() {
        if (inventory.getItemCount(CookedMeat.class) > 0) {
            inventory.remove(CookedMeat.class);
            heal(1, new CookedMeat());
        } else if (inventory.getItemCount(Meat.class) > 0) {
            inventory.remove(Meat.class);
            damage(1, new Meat());
        }
    }

    public void print() {
        section.print("Health: " + getHealth() + (kills > 0 ? " Kills: " + kills : "") + (invincible ? " ~INVINCIBLE~" : ""), Alignment.LEFT);
        inventory.print();
    }

    public void firePlayerLootCollectEvent(PlayerLootCollectEvent e) {
        System.out.println("firing player loot collect event");
        for (PlayerListener listener : listeners) {
            if (e.isCancelled()) {
                break;
            }
            listener.onPlayerLootCollect(e);
        }
    }

    public void firePlayerReachedExitEvent(PlayerReachedExitEvent e) {
        System.out.println("firing player reached exit event");
        for (PlayerListener listener : listeners) {
            if (e.isCancelled()) {
                break;
            }
            listener.onPlayerReachedExit(e);
        }
    }

    public void firePlayerDeathEvent(PlayerDeathEvent e) {
        System.out.println("firing player death event");
        for (PlayerListener listener : listeners) {
            if (e.isCancelled()) {
                break;
            }
            listener.onPlayerDeath(e);
        }
    }

    public void addPlayerListener(PlayerListener listener) {
        if (!listeners.contains(listener)) {
            listeners.add(listener);
        }
        if (!entityListeners.contains(listener)) {
            entityListeners.add(listener);
        }
    }

    public boolean removePlayerListener(PlayerListener listener) {
        entityListeners.remove(listener);
        return listeners.remove(listener);
    }

    public boolean toggleInvincible() {
        boolean b = invincible;
        invincible = !invincible;
        changed = true;
        return b;
    }

    public Color getForeground() {
        return FOREGROUND;
    }

    public Color getBackground() {
        return BACKGROUND;
    }

    public char getSymbol() {
        return SYMBOL;
    }

    public int getType() {
        return Level.PLAYER;
    }

    @Override
    public Point getPosition() {
        return new Point(x, y);
    }

    @Override
    public int getHealth() {
        return health;
    }

    @Override
    public void damage(int damage, Entity source) {
        if (damage < 0) throw new IllegalArgumentException("damage can not be < 0");
        if (invincible) {
            System.out.println("player was damaged by " + source + " but player is invincible");
            return;
        }
        health -= damage;
        if (damage > 0) {
            changed = true;
            fireEntityHitEvent(new EntityHitEvent(this, source, damage));
        }
    }

    @Override
    public void damage(int damage, Item source) {
        if (damage < 0) throw new IllegalArgumentException("damage can not be < 0");
        if (invincible) {
            System.out.println("player was damaged by " + source + " but player is invincible");
            return;
        }
        health -= damage;
        if (damage > 0) {
            changed = true;
            fireEntityHitEvent(new EntityHitEvent(this, source, damage));
        }
    }

    @Override
    public void damage(int damage, LevelComponent source) {
        if (damage < 0) throw new IllegalArgumentException("damage can not be < 0");
        if (invincible) {
            System.out.println("player was damaged by " + source + " but player is invincible");
            return;
        }
        health -= damage;
        if (damage > 0) {
            changed = true;
            fireEntityHitEvent(new EntityHitEvent(this, source, damage));
        }
    }

    @Override
    public void heal(int amount, Entity source) {
        if (amount < 0) throw new IllegalArgumentException("amount can not be < 0");
        health += amount;
        if (amount > 0) {
            changed = true;
            fireEntityHealedEvent(new EntityHealedEvent(this, source, amount));
        }
    }

    @Override
    public void heal(int amount, Item source) {
        if (amount < 0) throw new IllegalArgumentException("amount can not be < 0");
        health += amount;
        if (amount > 0) {
            changed = true;
            fireEntityHealedEvent(new EntityHealedEvent(this, source, amount));
        }
    }

    @Override
    public void move(int x, int y) {
        Point oldPosition = new Point(this.x, this.y);
        Point newPosition = new Point(this.x + x, this.y + y);

        // handle invalid values
        if (newPosition.x < 0) {
            newPosition = new Point(0, newPosition.y);
        } else if (newPosition.x >= level.getWidth()) {
            newPosition = new Point(level.getWidth() - 1, newPosition.y);
        }

        if (newPosition.y < 0) {
            newPosition = new Point(newPosition.x, 0);
        } else if (newPosition.y >= level.getHeight()) {
            newPosition = new Point(newPosition.x, level.getHeight() - 1);
        }

        LevelComponent component = level.get(newPosition.x, newPosition.y);
        if (!newPosition.equals(oldPosition) && component.canMoveOn()) {
            Entity other;
            if (component instanceof Loot) {
                firePlayerLootCollectEvent(new PlayerLootCollectEvent(this, (Loot) component, newPosition, level));
            } else if (component instanceof Exit) {
                firePlayerReachedExitEvent(new PlayerReachedExitEvent(this, oldPosition, newPosition, level));
            } else if (component instanceof Trap) {
                fireEntityStepOnTrapEvent(new EntityStepOnTrapEvent(this, (Trap) component, newPosition, level));
            } else if ((other = level.getEntities().getKey(newPosition)) != null) {
                fireEntityCollisionEvent(new EntityCollisionEvent(this, other));
                other.fireEntityCollisionEvent(new EntityCollisionEvent(other, this));
                return;
            }

            this.x += x;
            this.y += y;
            moved = true;
            fireEntityMovementEvent(new EntityMovementEvent(this, oldPosition, newPosition, level));
        }
    }

    @Override
    public Level getLevel() {
        return level;
    }

    public boolean hasChanged() {
        return changed || inventory.hasChanged();
    }

    @Override
    public boolean hasMoved() {
        return moved;
    }

    @Override
    public void forceReprint() {
        moved = true;
    }

    public boolean rearm() {
        boolean b = changed || inventory.rearm();
        changed = false;
        return b;
    }

    @Override
    public boolean rearmMovement() {
        boolean b = moved;
        moved = false;
        return b;
    }

    public void forceUpdate() {
        changed = true;
        inventory.forceUpdate();
    }

    @Override
    public void fireEntityHitEvent(EntityHitEvent e) {
        System.out.println("firing player hit event");
        for (EntityListener listener : entityListeners) {
            if (e.isCancelled()) {
                break;
            }
            listener.onEntityHit(e);
        }
    }

    @Override
    public void fireEntityHealedEvent(EntityHealedEvent e) {
        System.out.println("firing player healed event");
        for (EntityListener listener : entityListeners) {
            if (e.isCancelled()) {
                break;
            }
            listener.onEntityHealed(e);
        }
    }

    @Override
    public void fireEntityCollisionEvent(EntityCollisionEvent e) {
        System.out.println("firing player collision event");
        for (EntityListener listener : entityListeners) {
            if (e.isCancelled()) {
                break;
            }
            listener.onEntityCollision(e);
        }
    }

    @Override
    public void fireEntityMovementEvent(EntityMovementEvent e) {
        System.out.println("firing player movement event: " + e.getFrom().toString() + " -> " + e.getTo().toString());
        for (EntityListener listener : entityListeners) {
            if (e.isCancelled()) {
                break;
            }
            listener.onEntityMovement(e);
        }
    }

    @Override
    public void fireEntityStepOnTrapEvent(EntityStepOnTrapEvent e) {
        System.out.println("firing player step on trap event");
        for (EntityListener listener : entityListeners) {
            if (e.isCancelled()) {
                break;
            }
            listener.onEntityStepOnTrap(e);
        }
    }

    @Override
    public void fireEntityDeathEvent(EntityDeathEvent e) {
        System.out.println("firing player death event");
        for (EntityListener listener : entityListeners) {
            if (e.isCancelled()) {
                break;
            }
            listener.onEntityDeath(e);
        }
    }

    @Override
    public void addEntityListener(EntityListener l) {
        if (!entityListeners.contains(l)) {
            entityListeners.add(l);
        }
    }

    @Override
    public boolean removeEntityListener(EntityListener l) {
        return entityListeners.remove(l);
    }
}
