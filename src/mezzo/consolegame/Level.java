/*
 * LICENSE
 * 
 * This work is licensed under the Creative Commons Attribution-ShareAlike 4.0
 * International License. To view a copy of this license, visit
 * http://creativecommons.org/licenses/by-sa/4.0/.
 */
package mezzo.consolegame;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import mezzo.consolegame.entity.Entity;
import mezzo.consolegame.entity.Monster;
import mezzo.consolegame.entity.Player;
import mezzo.consolegame.gui.ConsoleFrame;
import mezzo.consolegame.level.Air;
import mezzo.consolegame.level.Entrance;
import mezzo.consolegame.level.Exit;
import mezzo.consolegame.level.LevelComponent;
import mezzo.consolegame.level.Loot;
import mezzo.consolegame.level.MonsterCorpse;
import mezzo.consolegame.level.Trap;
import mezzo.consolegame.level.Wall;
import mezzo.consolegame.level.generation.LevelGenerator.GeneratorSettings;
import mezzo.util.BidirectionalMap;
import mezzo.util.HashTable;
import mezzo.util.HashTable.Key2;
import mezzo.util.ReturnValue2;
import mezzo.util.pathfinding.astar.AStar;
import mezzo.util.pathfinding.astar.LocationNode;

/**
 * <code>Level</code>
 * 
 * @author Felix Fr�hlich <felix.froehlich@convey.de>
 */
public class Level {
    private Player                               player             = null;
    private List<Point>                          possibleTargets    = new ArrayList<>();
    private Map<LevelComponent, Point>           updates            = new LinkedHashMap<>();
    private BidirectionalMap<Monster, Point>     monsters           = new BidirectionalMap<>();
    private HashTable<Point, Point, PointNode[]> shortcuts          = new HashTable<>();
    private final int                            width;
    private final int                            height;
    private final int                            monsterHealth      = 2;
    private final LevelComponent[][]             map;
    private final PointNode[][]                  nodeMap;

    private static final Pattern                 PATTERN_COORDINATE = Pattern.compile("(\\d+),(\\d+)");
    private static final Pattern                 PATTERN_SHORTCUT   = Pattern.compile("(\\d+),(\\d+)->(\\\\d+),(\\\\d+)");
    private static final Pattern                 PATTERN_PATH       = Pattern.compile("(\\d+,\\d+)(->(\\d+,\\d+))*");

    public static final int                      AIR                = 0xffffffff;
    public static final int                      WALL               = 0x00000000;
    public static final int                      ENTRANCE           = 0x00000001;
    public static final int                      EXIT               = 0x00000002;
    public static final int                      TRAP               = 0x00000003;
    public static final int                      MONSTER            = 0x00000004;
    public static final int                      LOOT               = 0x00000005;
    public static final int                      PLAYER             = 0x00000006;
    public static final int                      MONSTER_CORPSE     = 0x00000007;

    public Level(Properties level, Properties shortcuts, ConsoleFrame consoleFrame) {
        width = Integer.valueOf(level.getProperty("Width"));
        height = Integer.valueOf(level.getProperty("Height"));
        map = new LevelComponent[width][height];
        nodeMap = new PointNode[width][height];

        System.out.println("before initialization");

        System.out.println("before property reading");
        boolean hasPlayer = false;
        Point playerLocation = null;
        List<Point> entrances = new ArrayList<Point>();
        long e = 0;
        long pl = 0;
        long s = 0;
        long t = 0;
        for (Object o : level.keySet()) {
            String key = o.toString();
            Matcher m = PATTERN_COORDINATE.matcher(key);
            if (m.matches()) {
                int x = Integer.valueOf(m.group(1));
                int y = Integer.valueOf(m.group(2));
                int type = Integer.valueOf(level.getProperty(key));
                Point cur = new Point(x, y);
                if (type == ENTRANCE) {
                    t = System.currentTimeMillis();
                    entrances.add(cur);
                    e += System.currentTimeMillis() - t;
                }
                if (type == PLAYER) {
                    t = System.currentTimeMillis();
                    hasPlayer = true;
                    playerLocation = cur;
                    pl += System.currentTimeMillis() - t;
                }
                t = System.currentTimeMillis();
                set(x, y, type);
                s += System.currentTimeMillis() - t;
            }
        }
        System.out.println(e + " " + pl + " " + s);

        System.out.println("before loading player");
        if (!hasPlayer) {
            playerLocation = entrances.get(new Random().nextInt(entrances.size()));
        }
        player = new Player(playerLocation.x, playerLocation.y, 3, this, consoleFrame.getTerminal());

        loadShortcuts(shortcuts);
        initPathfinding();
    }

    public Level(LevelComponent[][] map, BidirectionalMap<Monster, Point> monsters, HashTable<Point, Point, PointNode[]> shortcuts, ConsoleFrame consoleFrame, GeneratorSettings g) {
        width = g.width;
        height = g.height;
        this.map = new LevelComponent[width][height];
        nodeMap = new PointNode[width][height];

        boolean hasPlayer = false;
        Point playerLocation = null;
        List<Point> entrances = new ArrayList<Point>();
        long timeEntrances = 0;
        long timePlayer = 0;
        long timeSet = 0;
        long startTime = 0;
        for (int x = 0; x < map.length; x++) {
            for (int y = 0; y < map[x].length; y++) {
                LevelComponent c = map[x][y];
                Point curPoint = new Point(x, y);
                if (c instanceof Entrance) {
                    startTime = System.currentTimeMillis();
                    entrances.add(curPoint);
                    timeEntrances += System.currentTimeMillis() - startTime;
                }
                if (c instanceof Player) {
                    startTime = System.currentTimeMillis();
                    hasPlayer = true;
                    playerLocation = curPoint;
                    timePlayer += System.currentTimeMillis() - startTime;
                }
                startTime = System.currentTimeMillis();
                set(x, y, c.getType());
                timeSet += System.currentTimeMillis() - startTime;
            }
        }
        System.out.println(timeEntrances + " " + timePlayer + " " + timeSet);

        for (Entry<Monster, Point> entry : monsters.entrySet()) {
            Point p = entry.getValue();
            set(p.x, p.y, MONSTER);
        }

        System.out.println("before loading player");
        if (!hasPlayer) {
            playerLocation = entrances.get(new Random().nextInt(entrances.size()));
        }
        player = new Player(playerLocation.x, playerLocation.y, 3, this, consoleFrame.getTerminal());

        loadShortcuts(shortcuts);
        initPathfinding();
    }

    private void initPathfinding() {
        for (int x = 0; x < map.length; x++) {
            for (int y = 0; y < map[x].length; y++) {
                nodeMap[x][y] = new PointNode(new Point(x, y));
            }
        }
        for (int x = 0; x < nodeMap.length; x++) {
            for (int y = 0; y < nodeMap[x].length; y++) {
                if (get(x, y).canMoveOn()) {
                    possibleTargets.add(new Point(x, y));
                }
                if (x > 0 && get(x - 1, y).canMoveOn()) {
                    nodeMap[x][y].setDistance(nodeMap[x - 1][y], 1d);
                }
                if (x < map.length - 1 && get(x + 1, y).canMoveOn()) {
                    nodeMap[x][y].setDistance(nodeMap[x + 1][y], 1d);
                }
                if (y > 0 && get(x, y - 1).canMoveOn()) {
                    nodeMap[x][y].setDistance(nodeMap[x][y - 1], 1d);
                }
                if (y < map[x].length - 1 && get(x, y + 1).canMoveOn()) {
                    nodeMap[x][y].setDistance(nodeMap[x][y + 1], 1d);
                }
            }
        }

        Random r = new Random();
        for (Entry<Monster, Point> entry : monsters.entrySet()) {
            Monster monster = entry.getKey();
            monster.setCurrentPath(getShortestPath(monster.getPosition(), possibleTargets.get(r.nextInt(possibleTargets.size()))));
        }
    }

    private void loadShortcuts(HashTable<Point, Point, PointNode[]> shortcuts) {
        this.shortcuts = shortcuts;
    }

    public void loadShortcuts(Properties p) {
        for (Object o : p.keySet()) {
            String key = o.toString();
            Matcher m = PATTERN_SHORTCUT.matcher(key);
            if (m.matches()) {
                int xFrom = Integer.valueOf(m.group(1));
                int yFrom = Integer.valueOf(m.group(2));
                int xTo = Integer.valueOf(m.group(3));
                int yTo = Integer.valueOf(m.group(4));
                Point from = new Point(xFrom, yFrom);
                Point to = new Point(xTo, yTo);
                PointNode[] pathNodes = new PointNode[0];
                String path = p.getProperty(key);

                m = PATTERN_PATH.matcher(path);
                if (!m.matches()) {
                    continue;
                }

                String[] split = path.split("->");
                int counter = 0;
                for (String s : split) {
                    m = PATTERN_COORDINATE.matcher(s);
                    if (!m.matches()) {
                        continue;
                    }
                    if (counter >= pathNodes.length) {
                        pathNodes = Arrays.copyOf(pathNodes, counter + 1);
                    }
                    int x = Integer.valueOf(m.group(1));
                    int y = Integer.valueOf(m.group(2));
                    pathNodes[counter++] = nodeMap[x][y];
                }

                shortcuts.set(from, to, pathNodes);
            }
        }
    }

    public PointNode[] getShortestPath(Point from, Point to) {
        System.out.println("starting calculation... (" + from + " -> " + to + ")");
        long start = System.currentTimeMillis();
        LocationNode[] l = AStar.calculate(nodeMap[from.x][from.y], nodeMap[to.x][to.y]);
        System.out.println("done. took " + (System.currentTimeMillis() - start) + "ms");
        return Arrays.copyOf(l, l.length, PointNode[].class);
    }

    public List<Point> getPossibleTargets() {
        return possibleTargets;
    }

    public void set(int x, int y, int type) {
        LevelComponent old = get(x, y);
        Point p = new Point(x, y);
        switch (type) {
            case WALL:
                map[x][y] = new Wall();
                break;
            case ENTRANCE:
                map[x][y] = new Entrance();
                break;
            case EXIT:
                map[x][y] = new Exit();
                break;
            case TRAP:
                map[x][y] = new Trap();
                break;
            case MONSTER:
                Monster m = new Monster(x, y, monsterHealth, this);
                monsters.put(m, p);
                break;
            case LOOT:
                map[x][y] = new Loot();
                break;
            case MONSTER_CORPSE:
                map[x][y] = new MonsterCorpse();
                break;
            default:
                map[x][y] = null;
        }
        if (get(x, y) != old) {
            updates.put(get(x, y), new Point(x, y));
        }
    }

    public Player getPlayer() {
        return player;
    }

    public LevelComponent get(int x, int y) {
        return map[x][y] == null ? new Air() : map[x][y];
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public void reprint() {
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < width; y++) {
                updates.put(get(x, y), new Point(x, y));
            }
        }
    }

    public void reprint(int x, int y) {
        set(x, y, get(x, y).getType());
    }

    public Entry<LevelComponent, Point> nextUpdate() {
        for (Entry<LevelComponent, Point> entry : updates.entrySet()) {
            updates.remove(entry.getKey(), entry.getValue());
            return entry;
        }
        return null;
    }

    public Entry<LevelComponent, Point> nextUpdate(int x, int y, int width, int height) {
        for (Entry<LevelComponent, Point> entry : updates.entrySet()) {
            Point l = entry.getValue();
            if (l.x >= x && l.x < x + width && l.y >= y && l.y < y + height) {
                updates.remove(entry.getKey(), entry.getValue());
                return entry;
            }
        }
        return null;
    }

    public BidirectionalMap<Monster, Point> getMonsters() {
        return monsters;
    }

    public BidirectionalMap<Entity, Point> getEntities() {
        BidirectionalMap<Entity, Point> ret = new BidirectionalMap<>();
        ret.putAll(monsters);
        ret.put(player, player.getPosition());
        return ret;
    }

    public boolean removeEntity(Entity e) {
        if (e instanceof Monster) {
            Point location = monsters.remove(e);
            if (location == null) return false;
            reprint(location.x, location.y);
            return true;
        }
        return false;
    }

    public static ReturnValue2<Properties, Properties> toProperties(Level l) {
        Properties level = new Properties();
        level.setProperty("Width", String.valueOf(l.width));
        level.setProperty("Height", String.valueOf(l.height));

        for (int x = 0; x < l.map.length; x++) {
            for (int y = 0; y < l.map[x].length; y++) {
                if (new Point(x, y).equals(l.player.getPosition())) {
                    level.setProperty(x + "," + y, String.valueOf(PLAYER));
                } else if (l.get(x, y).getType() != AIR) {
                    level.setProperty(x + "," + y, String.valueOf(l.get(x, y).getType()));
                }
            }
        }

        Properties shortcuts = new Properties();
        for (Entry<Key2<Point, Point>, PointNode[]> entry : l.shortcuts.entrySet()) {
            Point from = entry.getKey().part1;
            Point to = entry.getKey().part2;
            PointNode[] path = entry.getValue();
            StringBuilder value = new StringBuilder();
            for (int i = 0; i < path.length; i++) {
                if (i != 0) {
                    value.append("->");
                }
                value.append(path[i].toPoint().x).append(",").append(path[i].toPoint().y);
            }
            shortcuts.setProperty(from.x + "," + from.y + "->" + to.x + "," + to.y, value.toString());
        }

        return new ReturnValue2<>(level, shortcuts);
    }

    public static class Point {
        public final int x;
        public final int y;

        public Point(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public double distanceTo(Point p) {
            return Math.sqrt(Math.pow(x - p.x, 2) + Math.pow(y - p.y, 2));
        }

        public boolean within(Point p, double radius) {
            return distanceTo(p) <= radius;
        }

        @Override
        public boolean equals(Object o) {
            if (!(o instanceof Point)) return false;
            Point l = (Point) o;
            return l.x == x && l.y == y;
        }

        @Override
        public String toString() {
            return getClass().getSimpleName() + "(" + x + "|" + y + ")";
        }

        @Override
        public int hashCode() {
            int hash = 1;
            hash = hash * 17 + x;
            hash = hash * 31 + y;
            return hash;
        }
    }

    public static class PointNode extends LocationNode {
        public PointNode(Point position) {
            super(position.x, position.y);
        }

        public Point toPoint() {
            return new Point((int) x, (int) y);
        }

        @Override
        public String toString() {
            return /* getClass().getSimpleName() + */"(" + x + "|" + y + ")";
        }

        public void print() {
            print(0, new ArrayList<>());
        }

        private void print(int indent, ArrayList<PointNode> visited) {
            visited.add(this);
            for (LocationNode node : getAdjacentNodes()) {
                if (visited.contains(node)) {
                    continue;
                }
                ((PointNode) node).print(indent + 2, visited);
            }
        }
    }
}
